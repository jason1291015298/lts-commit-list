# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.4.156
  - c40a7b3592b3 Linux 4.4.156

  ACK: iwamatsu

  - a632d2d1849f btrfs: use correct compare function of dirty_metadata_bytes
  ACK: iwamatsu, typo in commit log ([)

  - 116a6ad749e5 ASoC: wm8994: Fix missing break in switch
  ACK: iwamatsu
  - 280880cebc33 s390/lib: use expoline for all bcr instructions
  - 5597719733b8 mei: me: allow runtime pm for platform with D0i3
  ACK: iwamatsu
  - aa5d14953152 sch_tbf: fix two null pointer dereferences on init failure
  ACK: iwamatsu
  - 7a4eae7ae6f4 sch_netem: avoid null pointer deref on init failure
  ACK: iwamatsu
  - 9dafa62c8755 sch_hhf: fix null pointer dereference on init failure
  ACK: iwamatsu
  - 68858be0c1b5 sch_multiq: fix double free on init failure
  ACK: iwamatsu
  - 7edd04ddb3f3 sch_htb: fix crash on init failure
  ACK: iwamatsu
  - 89f15c6e8212 ovl: proper cleanup of workdir
  ACK: iwamatsu
  - 121b09d30d48 ovl: override creds with the ones from the superblock mounter
  ACK: iwamatsu
  - 6586f61ab5bb ovl: rename is_merge to is_lowest
  ACK: iwamatsu - This commit alone does not make sense, but it is necessary to apply oter ovl commit.

  - eadbe44f3978 irqchip/gic: Make interrupt ID 1020 invalid
  ACK: iwamatsu
  - 3107eb31aba1 irqchip/gic-v3: Add missing barrier to 32bit version of gic_read_iar()
  ACK: iwamatsu
  - 5e56ddc78f0e irqchip/gicv3-its: Avoid cache flush beyond ITS_BASERn memory size
  ACK: iwamatsu
  - 4a0c7f6afea7 irqchip/gicv3-its: Fix memory leak in its_free_tables()
  ACK: iwamatsu
  - ad37cd79428e irqchip/gic-v3-its: Recompute the number of pages on page size change
  ACK: iwamatsu
  - 27e83f7dd91d genirq: Delay incrementing interrupt count if it's disabled/pending
  ACK: iwamatsu
  - e72977e87482 Fixes: Commit cdbf92675fad ("mm: numa: avoid waiting on freed migrated pages")
  iwamatsu: wrong commit message
  - a50422747502 enic: do not call enic_change_mtu in enic_probe
  ACK: iwamatsu
  - a37c70426caa Revert "ARM: imx_v6_v7_defconfig: Select ULPI support"
  ACK: iwamatsu
  - 131a3b82c853 irda: Only insert new objects into the global database via setsockopt
  ACK: iwamatsu
  - 4a7811bb3ae1 irda: Fix memory leak caused by repeated binds of irda socket
  ACK: iwamatsu
  - accf294af418 kbuild: make missing $DEPMOD a Warning instead of an Error
  ACK: iwamatsu
  - f46d2b99a6ac x86/pae: use 64 bit atomic xchg function in native_ptep_get_and_clear
  ACK: iwamatsu
  - 98d122a4a746 debugobjects: Make stack check warning more informative
  iwamatsu: Add more information of debuginfo
  - 02e48c4d57cc btrfs: Don't remove block group that still has pinned down bytes
  ACK: iwamatsu
  - 510825b3f8c1 btrfs: relocation: Only remove reloc rb_trees if reloc control has been initialized
  ACK: iwamatsu
  - accb3e424b8b btrfs: replace: Reset on-disk dev stats value after replace
  ACK: iwamatsu
  - 2dc310f83754 powerpc/pseries: Avoid using the size greater than RTAS_ERROR_LOG_MAX.
  ACK: iwamatsu
  - a9997f887351 SMB3: Number of requests sent should be displayed for SMB3 not just CIFS
  ACK: iwamatsu
  - d6773f4061ce smb3: fix reset of bytes read and written stats
  ACK: iwamatsu
  - 8e676abeaf4b selftests/powerpc: Kill child processes on SIGINT

  iwamatsu - two fixes.  

  - 9ad681c4ba53 staging: comedi: ni_mio_common: fix subdevice flags for PFI subdevice

  ACK: iwamatsu

  - 66236f1b06f6 dm kcopyd: avoid softlockup in run_complete_job

  ACK: iwamatsu

  - d07bbe50d1b7 PCI: mvebu: Fix I/O space end address calculation

  ACK: iwamatsu

  - 242343ebf645 scsi: aic94xx: fix an error code in aic94xx_init()

  ACK: iwamatsu

  - 4057a20078fe s390/dasd: fix hanging offline processing due to canceled worker

  ACK: iwamatsu

  - f675ab001e78 powerpc: Fix size calculation using resource_size()

  ACK: iwamatsu

  - 75c55cbd46b1 net/9p: fix error path of p9_virtio_probe

  ACK: iwamatsu

  - 3537179891ca irqchip/bcm7038-l1: Hide cpu offline callback when building for !SMP

  ACK: iwamatsu

  - ad09041e9335 platform/x86: asus-nb-wmi: Add keymap entry for lid flip action on UX360

  iwamatsu - Not fix, add new keymap

  - a429a299eb48 mfd: sm501: Set coherent_dma_mask when creating subdevices

  ACK: iwamatsu

  - 4ebf605de623 ipvs: fix race between ip_vs_conn_new() and ip_vs_del_dest()
  - 90d91af02198 fs/dcache.c: fix kmemcheck splat at take_dentry_name_snapshot()

  ACK: iwamatsu

  - 4ca3b3df6d52 mm/fadvise.c: fix signed overflow UBSAN complaint

  ACK: iwamatsu

  - af25dc4cf354 scripts: modpost: check memory allocation results

  ACK: iwamatsu

  - 192710dab763 fat: validate ->i_start before using

  ACK: iwamatsu - fix with coding style.

  - fed5bd3352a3 hfsplus: fix NULL dereference in hfsplus_lookup()

  ACK: iwamatsu

  - ccbe4990bb1b reiserfs: change j_timestamp type to time64_t

  iwamatsu - not bugfix.

  - b7befd11e0b2 fork: don't copy inconsistent signal handler state to child

  ACK: iwamatsu

  - 189ff5b00004 hfs: prevent crash on exit from failed search

  ACK: iwamatsu

  - 14957e348e78 hfsplus: don't return 0 when fill_super() failed

  ACK: iwamatsu

  - d98ec8a9e205 cifs: check if SMB2 PDU size has been padded and suppress the warning

  ACK: iwamatsu

  - 4890349d7902 vti6: remove !skb->ignore_df check from vti6_xmit()

  ACK: iwamatsu

  - 86a0a00794c2 tcp: do not restart timewait timer on rst reception

  ACK: iwamatsu -- coding style is wrong

  - 375e88743c94 qlge: Fix netdev features configuration.

  ACK: iwamatsu

  - e1e4b0be0dec net: bcmgenet: use MAC link status for fixed phy

  ACK: iwamatsu

  - 2c155709e4ef staging: android: ion: fix ION_IOC_{MAP,SHARE} use-after-free

  ACK: iwamatsu

  - e3dea38fc852 x86/speculation/l1tf: Fix up pte->pfn conversion for PAE

  ACK: iwamatsu

