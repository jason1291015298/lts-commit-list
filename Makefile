#! /usr/bin/make -f
# -*- makefile -*-

# It is useful to do "git remote update" in the repository before running this
REPO:=../kernel
LATEST_4_4=$(shell cd $(REPO); git tag -l v4.4.* --sort=version:refname | tail -1 | sed -e 's/v4.4.//g')
LATEST_4_19=$(shell cd $(REPO); git tag -l v4.19.*  --sort=version:refname | tail -1 | sed -e 's/v4.19.//g')
TARGETVER:=v4.4 v4.19

define create_commit_list
	ver=$2 ; \
	while [ $$ver -lt $3 ] ; do \
		ver_s=$$ver ; \
		ver=$$((ver+1)) ; \
		if [ -e $(PWD)/linux-kernel-$1.$$ver-commit.list ] ; then \
			continue ; \
		fi ; \
		cat  $(PWD)/header > $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		echo "## $1.$$ver" >>  $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		if [ $$ver_s -eq 0 ] ; then \
			git log --oneline $1...$1.$$ver | sed -e "s/^/  - /g" >> $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		else \
			git log --oneline $1.$$ver_s...$1.$$ver | sed -e "s/^/  - /g" >> $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		fi \
	done
endef

all: $(TARGETVER)
v4.4: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,154,${LATEST_4_4})

v4.19: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,0,${LATEST_4_19})
