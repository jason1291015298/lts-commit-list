# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.190
  - 5e9f4d7 Linux 4.4.190
  - a0f592c bonding: Add vlan tx offload to hw_enc_features
  - de39605 sctp: fix the transport error_count check
  - 6ee6bf5 net/mlx5e: Only support tx/rx pause setting for port owner
  - 4a6590c xen/netback: Reset nr_frags before freeing skb
  - 8c7d8f6 net/packet: fix race in tpacket_snd()
  - 87521db x86/boot: Disable the address-of-packed-member compiler warning
  - 54777f1 iommu/amd: Move iommu_init_pci() to .init section
  - d96d878 x86/vdso: Remove direct HPET access through the vDSO
  - de4ee65 IB/mlx5: Make coding style more consistent
  - 7690f64 RDMA: Directly cast the sockaddr union to sockaddr
  - 483b260 scsi: fcoe: Embed fc_rport_priv in fcoe_rport structure
  - 1ac31ce asm-generic: default BUG_ON(x) to if(x)BUG()
  - 4f68628 Input: psmouse - fix build error of multiple definition
  - 2b90ebd arm64: compat: Allow single-byte watchpoints on all addresses
  - 170051d include/linux/module.h: copy __init/__exit attrs to init/cleanup_module
  - edc966d Backport minimal compiler_attributes.h to support GCC 9
  - 8a2d56e USB: serial: option: Add Motorola modem UARTs
  - 661d34d USB: serial: option: add the BroadMobi BM818 card
  - bd9ae29 USB: serial: option: Add support for ZTE MF871A
  - 9aa449d USB: serial: option: add D-Link DWM-222 device ID
  - 8fed007 usb: cdc-acm: make sure a refcount is taken early enough
  - 8f67bc7 USB: core: Fix races in character device registration and deregistraion
  - 6bf6e04 staging: comedi: dt3000: Fix rounding up of timer divisor
  - 893e2db staging: comedi: dt3000: Fix signed integer overflow 'divider * base'
  - dbc6807 asm-generic: fix -Wtype-limits compiler warnings
  - 738515e ocfs2: remove set but not used variable 'last_hash'
  - b88f8f1 IB/core: Add mitigation for Spectre V1
  - 81882762 kbuild: modpost: handle KBUILD_EXTRA_SYMBOLS only for external modules
  - 7b31030 ata: libahci: do not complain in case of deferred probe
  - ef4eab8 scsi: hpsa: correct scsi command status issue after reset
  - 1ea16aa libata: zpodd: Fix small read overflow in zpodd_get_mech_type()
  - 0275d33 perf header: Fix use of unitialized value warning
  - a005fcf perf header: Fix divide by zero error if f_header.attr_size==0
  - 88d6588 irqchip/irq-imx-gpcv2: Forward irq type to parent
  - 4590398 xen/pciback: remove set but not used variable 'old_state'
  - 98ed73a net: usb: pegasus: fix improper read if get_registers() fail
  - 45bf48c Input: iforce - add sanity checks
  - be5ec20 Input: kbtab - sanity check for endpoint type
  - 8e73f43 HID: hiddev: do cleanup in failure of opening a device
  - 76285c8 HID: hiddev: avoid opening a disconnected device
  - a5ca282 HID: holtek: test for sanity of intfdata
  - ba8eaaf ALSA: hda - Fix a memory leak bug
  - 3d29e64 mm/memcontrol.c: fix use after free in mem_cgroup_iter()
  - 3dae85a USB: gadget: f_midi: fixing a possible double-free in f_midi
  - 4ccd064 usb: gadget: f_midi: fail if set_alt fails to allocate requests
  - 52a9592 sh: kernel: hw_breakpoint: Fix missing break in switch statement
  - 9841b51 scsi: mpt3sas: Use 63-bit DMA addressing on SAS35 HBA
  - 3475e0c mwifiex: fix 802.11n/WPA detection
  - c504c4b smb3: send CAP_DFS capability during session setup
  - 4474cea SMB3: Fix deadlock in validate negotiate hits reconnect
  - af9fbec mac80211: don't WARN on short WMM parameters from AP
  - d412fd0 ALSA: firewire: fix a memory leak bug
  - f4bf066 hwmon: (nct7802) Fix wrong detection of in4 presence
  - 6711294 can: peak_usb: pcan_usb_fd: Fix info-leaks to USB devices
  - abea9fa can: peak_usb: pcan_usb_pro: Fix info-leaks to USB devices
  - 821bc02 perf/core: Fix creating kernel counters for PMUs that override event->cpu
  - cef8869 tty/ldsem, locking/rwsem: Add missing ACQUIRE to read_failed sleep loop
  - d9974d9 scsi: ibmvfc: fix WARN_ON during event pool release
  - 7c7404e scsi: megaraid_sas: fix panic on loading firmware crashdump
  - 6e6026c ARM: davinci: fix sleep.S build error on ARMv4
  - c376212 perf probe: Avoid calling freeing routine multiple times for same pointer
  - b9cbcf7 ALSA: compress: Be more restrictive about when a drain is allowed
  - f9c5361 ALSA: compress: Prevent bypasses of set_params
  - 4e07b0f ALSA: compress: Fix regression on compressed capture streams
  - f34ef4f s390/qdio: add sanity checks to the fast-requeue path
  - e1c7e4e cpufreq/pasemi: fix use-after-free in pas_cpufreq_cpu_init()
  - dad7a50 hwmon: (nct6775) Fix register address and added missed tolerance for nct6106
  - ad08db2 mac80211: don't warn about CW params when not using them
  - 0487be5 iscsi_ibft: make ISCSI_IBFT dependson ACPI instead of ISCSI_IBFT_FIND
  - fbed278 netfilter: nfnetlink: avoid deadlock due to synchronous request_module
  - d199791 can: peak_usb: fix potential double kfree_skb()
  - 2ef494a usb: yurex: Fix use-after-free in yurex_delete
  - 347f5f0 perf db-export: Fix thread__exec_comm()
  - a89f96d mm/vmalloc: Sync unmappings in __purge_vmap_area_lazy()
  - 547922a x86/mm: Sync also unmappings in vmalloc_sync_all()
  - dc2b262 x86/mm: Check for pfn instead of page in vmalloc_sync_one()
  - 72db67d sound: fix a memory leak bug
  - 334bcf6 usb: iowarrior: fix deadlock on disconnect
