# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.4.162
  - 24c2342b8e51 Linux 4.4.162
  - e34c69f2094a HV: properly delay KVP packets when negotiation is in progress
  - 43fea648af71 Drivers: hv: kvp: fix IP Failover
  - eaee7976cf2a Drivers: hv: util: Pass the channel information during the init call
  - f91b8b1fd69c Drivers: hv: utils: Invoke the poll function after handshake
  - 68a318670c66 usb: gadget: serial: fix oops when data rx'd after close
  - 466dda64e443 ARC: build: Get rid of toolchain check

  IGN: iwamatsu

  - 5a51c55493c0 powerpc/tm: Avoid possible userspace r1 corruption on reclaim
  - 6939b3b00c12 powerpc/tm: Fix userspace r13 corruption
  - 0d02fdd080d2 net/mlx4: Use cpumask_available for eq->affinity_mask
  - 6df8eb6ae229 Input: atakbd - fix Atari CapsLock behaviour
  - d200ea29009e Input: atakbd - fix Atari keymap
  - be7e497bf090 clocksource/drivers/ti-32k: Add CLOCK_SOURCE_SUSPEND_NONSTOP flag for non-am43 SoCs
  - 2a585d445050 media: af9035: prevent buffer overflow on write
  - 4e7ef8fc8001 x86/fpu: Finish excising 'eagerfpu'
  - 6e7d7bea15e8 x86/fpu: Remove struct fpu::counter
  - 8c6b69cf4bd3 x86/fpu: Remove use_eager_fpu()
  - 84037eebce3a KVM: x86: remove eager_fpu field of struct kvm_vcpu_arch
  - dcea9310efa8 rtnl: limit IFLA_NUM_TX_QUEUES and IFLA_NUM_RX_QUEUES to 4096
  - 59b6d2e59b29 net: systemport: Fix wake-up interrupt race during resume
  - 2b2c9306e3cd net: mvpp2: Extract the correct ethtype from the skb for tx csum offload
  - e0fff89962d3 team: Forbid enslaving team device to itself
  - 2f04cacdcd5b qlcnic: fix Tx descriptor corruption on 82xx devices
  - 0aa059ea2dac net/usb: cancel pending work when unbinding smsc75xx
  - 1f3a2366922b netlabel: check for IPV4MASK in addrinfo_get
  - 9ee4a60d618e net/ipv6: Display all addresses in output of /proc/net/if_inet6
  - 2b7e4c735933 net: ipv4: update fnhe_pmtu when first hop's MTU changes
  - d7148eeb647f ipv4: fix use-after-free in ip_cmsg_recv_dstaddr()
  - f9d3572816ba ip_tunnel: be careful when accessing the inner header
  - 20f16d1a3817 ip6_tunnel: be careful when accessing the inner header
  - 2691921c7d26 bonding: avoid possible dead-lock
  - 4348b96650bb bnxt_en: Fix TX timeout during netpoll.
  - 5ac147ebfa1c jffs2: return -ERANGE when xattr buffer is too small

  ACK: iwamatsu

  - f37a3ff6e4c7 xhci: Don't print a warning when setting link state for disabled ports

  ACK: iwamatsu

  - 863334098b60 i2c: i2c-scmi: fix for i2c_smbus_write_block_data

  ACK: iwamatsu

  - 49f80e6ef438 perf script python: Fix export-to-postgresql.py occasional failure

  ACK: iwamatsu

  - 2f41f387b0f4 mach64: detect the dot clock divider correctly on sparc

  IGN: iwamatsu

  - 0f0ad8d54858 mm/vmstat.c: fix outdated vmstat_text

  ACK: iwamatsu

  - 7a3d844f83d4 ext4: add corruption check in ext4_xattr_set_entry()

  ACK: iwamatsu

  - d6a6d4e4f784 drm/amdgpu: Fix SDMA HQD destroy error on gfx_v7

  ACK: iwamatsu

  - 51b6ff44c6d4 ARM: dts: at91: add new compatibility string for macb on sama5d3

  ACK: iwamatsu

  - f74eda8ed5d9 net: macb: disable scatter-gather for macb on sama5d3

  ACK: iwamatsu

  - bd66767a8c0e stmmac: fix valid numbers of unicast filter entries

  ACK: iwamatsu

  - ba996b37908c sound: enable interrupt after dma buffer initialization

  ACK: iwamatsu

  - 8c2077219e25 mfd: omap-usb-host: Fix dts probe of children

  ACK: iwamatsu

  - ac38645226f3 selftests/efivarfs: add required kernel configs

  ACK: iwamatsu, This only affects the test.

  - a5aa8bd85a4f ASoC: sigmadsp: safeload should not have lower byte limit

  ACK: iwamatsu

  - 1e3a440cf2bd ASoC: wm8804: Add ACPI support

  ACK: iwamatsu
