# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.244
  - b71e57af961f Linux 4.4.244
  - 4000c1b82008 Convert trailing spaces and periods in path components
  - 428f824e9df4 ext4: fix leaking sysfs kobject after failed mount
  - 0b2a9f794fea reboot: fix overflow parsing reboot cpu number
  - a48b733bf4a7 Revert "kernel/reboot.c: convert simple_strtoul to kstrtoint"
  - 17b235e6cc9d perf/core: Fix race in the perf_mmap_close() function
  - ea31e9d93204 xen/events: block rogue events for some time
  - 732d4f2eb416 xen/events: defer eoi in case of excessive number of events
  - 26a6cbe30a2b xen/events: use a common cpu hotplug hook for event channels
  - 957a6b5f628b xen/events: switch user event channels to lateeoi model
  - fc1f51b7ab49 xen/pciback: use lateeoi irq binding
  - deac27752388 xen/scsiback: use lateeoi irq binding
  - fffc1ca5ed0e xen/netback: use lateeoi irq binding
  - a0eff570f98e xen/blkback: use lateeoi irq binding
  - 1d762cb6676b xen/events: add a new "late EOI" evtchn framework
  - 2e76c99ffbf9 xen/events: fix race in evtchn_fifo_unmask()
  - 33f38ccfe479 xen/events: add a proper barrier to 2-level uevent unmasking
  - c0e77192829a xen/events: avoid removing an event channel while handling it
  - dd55e75a4d39 x86/speculation: Allow IBPB to be conditionally enabled on CPUs with always-on STIBP
  - 09f5820cfdda random32: make prandom_u32() output unpredictable
  - 36d7b99b7a7d net: Update window_clamp if SOCK_RCVBUF is set
  - 8c6a13c26902 net/x25: Fix null-ptr-deref in x25_connect
  - 480c6b7c69fd net/af_iucv: fix null pointer dereference on shutdown
  - c22894e51dec IPv6: Set SIT tunnel hard_header_len to zero
  - 18bfc9ef8b93 swiotlb: fix "x86: Don't panic if can not alloc buffer for swiotlb"
  - cd02ef5ba7a7 pinctrl: amd: fix incorrect way to disable debounce filter
  - 9ba5d0977edd pinctrl: amd: use higher precision for 512 RtcClk
  - 15b58f637512 drm/gma500: Fix out-of-bounds access to struct drm_device.vblank[]
  - 55c181227c19 don't dump the threads that had been already exiting when zapped.
  - 79de2e8400e9 ocfs2: initialize ip_next_orphan
  - c9d1594f0fbc mei: protect mei_cl_mtu from null dereference
  - e6c6322e1ce0 usb: cdc-acm: Add DISABLE_ECHO for Renesas USB Download mode
  - 9fbacd44844c ext4: unlock xattr_sem properly in ext4_inline_data_truncate()
  - 28f2350893a0 ext4: correctly report "not supported" for {usr,grp}jquota when !CONFIG_QUOTA
  - 57f9654bde66 perf: Fix get_recursion_context()
  - 3c9d5cf886fa cosa: Add missing kfree in error path of cosa_write
  - 2e4d9fac404b of/address: Fix of_node memory leak in of_dma_is_coherent
  - bf19a74c7e33 xfs: fix a missing unlock on error in xfs_fs_map_blocks
  - 37081c901206 iommu/amd: Increase interrupt remapping table limit to 512 entries
  - 82c5a4fd2044 cfg80211: regulatory: Fix inconsistent format argument
  - 03ca13c58060 mac80211: fix use of skb payload instead of header
  - 05b8a031aa8f drm/amdgpu: perform srbm soft reset always on SDMA resume
  - 09537f7f0cd1 gfs2: check for live vs. read-only file system in gfs2_fitrim
  - 89d2da2495f0 gfs2: Free rd_bits later in gfs2_clear_rgrpd to fix use-after-free
  - 71e893cf15d2 usb: gadget: goku_udc: fix potential crashes in probe
  - b05de537d423 ath9k_htc: Use appropriate rs_datalen type
  - e98bcb652998 geneve: add transport ports in route lookup for geneve
  - 78b5c1e88af9 i40e: Fix of memory leak and integer truncation in i40e_virtchnl.c
  - 0f60ed1afff4 i40e: Wrong truncation from u16 to u8
  - 03f69244302d pinctrl: devicetree: Avoid taking direct reference to device name string
  - b8b68014bfe6 Btrfs: fix missing error return if writeback for extent buffer never started
  - a17fd24a24bd can: peak_usb: peak_usb_get_ts_time(): fix timestamp wrapping
  - 261202518ab6 can: peak_usb: add range checking in decode operations
  - 1164646e69d9 can: can_create_echo_skb(): fix echo skb generation: always use skb_clone()
  - 0b7578675c2a can: dev: __can_get_echo_skb(): fix real payload length return value for RTR frames
  - 248b71ce92d4 can: dev: can_get_echo_skb(): prevent call to kfree_skb() in hard IRQ context
  - f1a6e0d012c9 ALSA: hda: prevent undefined shift in snd_hdac_ext_bus_get_link()
  - d990e768f7c2 perf tools: Add missing swap for ino_generation
  - c81542e3308f net: xfrm: fix a race condition during allocing spi
  - 9ce097a4ed94 btrfs: reschedule when cloning lots of extents
  - 9cddbb8787ef time: Prevent undefined behaviour in timespec64_to_ns()
  - 45b52deae851 mm: mempolicy: fix potential pte_unmap_unlock pte error
  - 78933fded91d gfs2: Wake up when sd_glock_disposal becomes zero
  - 47f1d1fe0cfa ring-buffer: Fix recursion protection transitions between interrupt context
