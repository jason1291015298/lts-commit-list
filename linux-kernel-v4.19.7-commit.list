# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.7
  - 61c68f2a2af0 Linux 4.19.7
  - 842c4c22ea2b misc: mic/scif: fix copy-paste error in scif_create_remote_lookup
  - 5e4b30d68a02 Drivers: hv: vmbus: check the creation_status in vmbus_establish_gpadl()
  - b66375b599bf mm: use swp_offset as key in shmem_replace_page()
  - 16a2d602244f mm: cleancache: fix corruption on missed inode invalidation
  - 91a514788356 lib/test_kmod.c: fix rmmod double free
  - 855f9dc87160 iio:st_magn: Fix enable device after trigger
  - ec800c8b028e iio/hid-sensors: Fix IIO_CHAN_INFO_RAW returning wrong values for signed numbers
  - 91f1c5c65d73 Revert "usb: dwc3: gadget: skip Set/Clear Halt when invalid"
  - c7d370712e72 usb: core: quirks: add RESET_RESUME quirk for Cherry G230 Stream series
  - d4f924e359ad USB: usb-storage: Add new IDs to ums-realtek
  - b73301b7db4a staging: rtl8723bs: Add missing return for cfg80211_rtw_get_station
  - 6d956674dfc5 staging: rtl8723bs: Fix incorrect sense of ether_addr_equal
  - fa299861aeeb staging: mt7621-pinctrl: fix uninitialized variable ngroups
  - bea52e4d1e42 staging: mt7621-dma: fix potentially dereferencing uninitialized 'tx_desc'
  - 6df2b837939a staging: vchiq_arm: fix compat VCHIQ_IOC_AWAIT_COMPLETION
  - 053b783d3da3 staging: most: use format specifier "%s" in snprintf
  - 0d04d450fe0d dmaengine: at_hdmac: fix module unloading
  - 9983a5bba09e dmaengine: at_hdmac: fix memory leak in at_dma_xlate()
  - 6e74fc22db9c ARM: dts: rockchip: Remove @0 from the veyron memory node
  - b16c2b78f13c ASoC: pcm186x: Fix device reset-registers trigger value
  - f35f68c68ce4 ASoC: intel: cht_bsw_max98090_ti: Add quirk for boards using pmc_plt_clk_0
  - ffaaaf68be1e ext2: fix potential use after free
  - 1666cf8c492d ext2: initialize opts.s_mount_opt as zero before using it
  - 553927d6aab9 binder: fix race that allows malicious free of live buffer
  - a22ff9df71aa function_graph: Reverse the order of pushing the ret_stack and the callback
  - d2bcf809e838 function_graph: Move return callback before update of curr_ret_stack
  - aec14c812ba8 function_graph: Have profiler use curr_ret_stack and not depth
  - 392374326d29 function_graph: Use new curr_ret_depth to manage depth instead of curr_ret_stack
  - 72c33b233f05 function_graph: Make ftrace_push_return_trace() static
  - 35aa93cbb04c MIPS: function_graph: Simplify with function_graph_enter()
  - bdfd01cfd1e7 arm64: function_graph: Simplify with function_graph_enter()
  - ef9326a145cd s390/function_graph: Simplify with function_graph_enter()
  - 84d2023c14ea riscv/function_graph: Simplify with function_graph_enter()
  - 87352d621bd3 parisc: function_graph: Simplify with function_graph_enter()
  - 34773b2f57ae sparc/function_graph: Simplify with function_graph_enter()
  - 56c1dd92c0fc sh/function_graph: Simplify with function_graph_enter()
  - 5478648ee825 powerpc/function_graph: Simplify with function_graph_enter()
  - 25ac02d0ed5e nds32: function_graph: Simplify with function_graph_enter()
  - 217614993ace x86/function_graph: Simplify with function_graph_enter()
  - e7deeabe9aa3 microblaze: function_graph: Simplify with function_graph_enter()
  - fbbee0cfba5f ARM: function_graph: Simplify with function_graph_enter()
  - 67d7bec3fc6c function_graph: Create function_graph_enter() to consolidate architecture code
  - b72fc1c3cce4 ALSA: hda/realtek - Add auto-mute quirk for HP Spectre x360 laptop
  - dcd51305cd41 ALSA: hda/realtek - fix the pop noise on headphone for lenovo laptops
  - 524841156bbe ALSA: hda/realtek - fix headset mic detection for MSI MS-B171
  - 094c00891be8 ALSA: hda/realtek - Support ALC300
  - bb951d8d965c ALSA: hda: Add ASRock N68C-S UCC the power_save blacklist
  - 15c5fb33cda9 ALSA: sparc: Fix invalid snd_free_pages() at error path
  - d8a2dca04064 ALSA: control: Fix race between adding and removing a user element
  - b77c35ef8e38 ALSA: ac97: Fix incorrect bit shift at AC97-SPSA control write
  - e83c4405ebf0 ALSA: wss: Fix invalid snd_free_pages() at error path
  - adcd35a38cde fs: fix lost error code in dio_complete
  - 205af59e7011 perf/x86/intel: Disallow precise_ip on BTS events
  - be0e2e2436cc perf/x86/intel: Add generic branch tracing check to intel_pmu_has_bts()
  - ad65b548409c perf/x86/intel: Move branch tracing setup to the Intel-specific source file
  - 33448a8b577d x86/fpu: Disable bottom halves while loading FPU registers
  - 00f91adf52af x86/MCE/AMD: Fix the thresholding machinery initialization order
  - 8af02415638e arm64: dts: rockchip: Fix PCIe reset polarity for rk3399-puma-haikou.
  - ab7702162bbc PCI: Fix incorrect value returned from pcie_get_speed_cap()
  - 1ce69ec307fd PCI: dwc: Fix MSI-X EP framework address calculation bug
  - b391ed731b39 PCI: layerscape: Fix wrong invocation of outbound window disable accessor
  - 590657656bdc btrfs: relocation: set trans to be NULL after ending transaction
  - 172a94eb2df2 Btrfs: fix race between enabling quotas and subvolume creation
  - 715608dbf756 Btrfs: fix rare chances for data loss when doing a fast fsync
  - 78a2890fcda1 Btrfs: ensure path name is null terminated at btrfs_control_ioctl
  - aaf249e36785 btrfs: Always try all copies when reading extent buffers
  - 949ddf8039bf udf: Allow mounting volumes with incorrect identification strings
  - 01fb21bf2424 xtensa: fix coprocessor part of ptrace_{get,set}xregs
  - 5f84a996a0cb xtensa: fix coprocessor context offset definitions
  - 4ec1039f474b xtensa: enable coprocessors that are being flushed
  - bbe23c4b948f KVM: VMX: re-add ple_gap module parameter
  - 61c42d657c85 KVM: X86: Fix scan ioapic use-before-initialization
  - ffb01e73737b KVM: LAPIC: Fix pv ipis use-before-initialization
  - 6d772df4038e KVM: x86: Fix kernel info-leak in KVM_HC_CLOCK_PAIRING hypercall
  - 76c8476cc720 KVM: nVMX/nSVM: Fix bug which sets vcpu->arch.tsc_offset to L1 tsc_offset
  - b8b0c871640f kvm: svm: Ensure an IBPB on all affected CPUs when freeing a vmcb
  - 471aca572529 kvm: mmu: Fix race in emulated page table writes
  - 34b7a7cc5321 userfaultfd: shmem/hugetlbfs: only allow to register VM_MAYWRITE vmas
  - 9f3baacee183 x86/speculation: Provide IBPB always command line options
  - d1ec2354787a x86/speculation: Add seccomp Spectre v2 user space protection mode
  - 7b62ef142c11 x86/speculation: Enable prctl mode for spectre_v2_user
  - 238ba6e75855 x86/speculation: Add prctl() control for indirect branch speculation
  - f67fafb88c46 x86/speculation: Prepare arch_smt_update() for PRCTL mode
  - e84124017450 x86/speculation: Prevent stale SPEC_CTRL msr content
  - 59028be13385 x86/speculation: Split out TIF update
  - aecb99692a4d ptrace: Remove unused ptrace_may_access_sched() and MODE_IBRS
  - a17888154cf2 x86/speculation: Prepare for conditional IBPB in switch_mm()
  - dd73e15ea213 x86/speculation: Avoid __switch_to_xtra() calls
  - a87c81f0de3d x86/process: Consolidate and simplify switch_to_xtra() code
  - 69985a2cae82 x86/speculation: Prepare for per task indirect branch speculation control
  - 711875432218 x86/speculation: Add command line control for indirect branch speculation
  - 8a34c70686e0 x86/speculation: Unify conditional spectre v2 print functions
  - 507ac84e159b x86/speculataion: Mark command line parser data __initdata
  - 275d90f0fa5d x86/speculation: Mark string arrays const correctly
  - 10c24dc62a3a x86/speculation: Reorder the spec_v2 code
  - 1572793b498d x86/l1tf: Show actual SMT state
  - f55e301ec4d5 x86/speculation: Rework SMT state change
  - 340693ee912e sched/smt: Expose sched_smt_present static key
  - f98bf1640e06 x86/Kconfig: Select SCHED_SMT if SMP enabled
  - a2c094816f89 sched/smt: Make sched_smt_present track topology
  - 6308dde50fcc x86/speculation: Reorganize speculation control MSRs update
  - 39402a5e5a29 x86/speculation: Rename SSBD update functions
  - ca41d792a230 x86/speculation: Disable STIBP when enhanced IBRS is in use
  - f37b55ab8d84 x86/speculation: Move STIPB/IBPB string conditionals out of cpu_show_common()
  - 30a8e214606d x86/speculation: Remove unnecessary ret variable in cpu_show_common()
  - 2ee44e2d761a x86/speculation: Clean up spectre_v2_parse_cmdline()
  - e8494e5df78e x86/speculation: Update the TIF_SSBD comment
  - 90d2c53fa196 x86/retpoline: Remove minimal retpoline support
  - 8c4ad5d39144 x86/retpoline: Make CONFIG_RETPOLINE depend on compiler support
  - cbc93677ef08 x86/speculation: Add RETPOLINE_AMD support to the inline asm CALL_NOSPEC variant
  - 44a076100a7b x86/speculation: Propagate information about RSB filling mitigation to sysfs
  - cacd9385b78d x86/speculation: Apply IBPB more strictly to avoid cross-process data leak
  - b07fc04c94e3 x86/speculation: Enable cross-hyperthread spectre v2 STIBP mitigation
  - 4e3fbd7433d7 tipc: fix lockdep warning during node delete
  - 38af4b903210 net: phy: add workaround for issue where PHY driver doesn't bind to the device
  - aaa7e45c00d6 tcp: defer SACK compression after DupThresh
  - b8e076958a09 net/dim: Update DIM start sample after each DIM iteration
  - b06510bf095b virtio-net: fail XDP set if guest csum is negotiated
  - 1af400beef4a virtio-net: disable guest csum during XDP set
  - 4aaa233c79f6 net: skb_scrub_packet(): Scrub offload_fwd_mark
  - 2f6cfb8e9f5c net: thunderx: set xdp_prog to NULL if bpf_prog_add fails
  - 535b494a4fa5 usbnet: ipheth: fix potential recvmsg bug and recvmsg bug 2
  - 711e3d37275c s390/qeth: fix length check in SNMP processing
  - 720e0d05af0f rapidio/rionet: do not free skb before reading its length
  - f2a67e68dbae packet: copy user buffers before orphan or clone
  - abc963e459fd net: thunderx: set tso_hdrs pointer to NULL in nicvf_free_snd_queue
  - cfbee9e96d79 net: gemini: Fix copy/paste error
  - b24a813e792d net: don't keep lonely packets forever in the gro hash
  - 18dd9bf51325 lan743x: fix return value for lan743x_tx_napi_poll
  - 767d890331ea lan743x: Enable driver to work with LAN7431
  - 8b37c40503ea mm/khugepaged: collapse_shmem() do not crash on Compound
  - af24c01831e4 mm/khugepaged: collapse_shmem() without freezing new_page
  - 3e9646c76cb9 mm/khugepaged: minor reorderings in collapse_shmem()
  - ee13d69bc1e8 mm/khugepaged: collapse_shmem() remember to clear holes
  - 78141aabfbb9 mm/khugepaged: fix crashes due to misaccounted holes
  - 8797f2f4fe0d mm/khugepaged: collapse_shmem() stop if punched or truncated
  - d31ff4722f45 mm/huge_memory: fix lockdep complaint on 32-bit i_size_read()
  - 7e18656c9a2c mm/huge_memory: splitting set mapping+index before unfreeze
  - 69697e6a61c7 mm/huge_memory: rename freeze_page() to unmap_page()
