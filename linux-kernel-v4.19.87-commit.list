# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.87
  - 174651bdf802 Linux 4.19.87
  - 6938a9da7ff8 PM / devfreq: Fix kernel oops on governor module load
  - 345712c95eec KVM: PPC: Book3S HV: Flush link stack on guest exit to host kernel
  - 0a60d4bddc0b powerpc/book3s64: Fix link stack flush on context switch
  - 19d98b4d55b0 powerpc/64s: support nospectre_v2 cmdline option
  - b7e2a040d9fc staging: comedi: usbduxfast: usbduxfast_ai_cmdtest rounding error
  IGN: pavel
  - 4101916e9f08 USB: serial: option: add support for Foxconn T77W968 LTE modules
  ACK: pavel
  - 62aca6645c08 USB: serial: option: add support for DW5821e with eSIM support
  ACK: pavel
  - 3349ed266ae2 USB: serial: mos7840: fix remote wakeup
  ACK: pavel -- bug from 2.6, and only affects power
  - abbda35d6a6e USB: serial: mos7720: fix remote wakeup
  ACK: pavel -- bug from 2.6, and only affects power
  - 84743898d60b USB: serial: mos7840: add USB ID to support Moxa UPort 2210
  ACK: pavel
  - 356440a79b6b appledisplay: fix error handling in the scheduled work
  ACK: pavel
  - 0439d6b90187 USB: chaoskey: fix error case of a timeout
  ACK: pavel
  - a18675e5d5f7 usb-serial: cp201x: support Mark-10 digital force gauge
  ACK: pavel
  - 61f6a3fac394 usbip: Fix uninitialized symbol 'nents' in stub_recv_cmd_submit()
  UR: pavel
  - 375b26a86481 usbip: tools: fix fd leakage in the function of read_attr_usbip_status
  ACK: pavel
  - e70448b92253 USBIP: add config dependency for SGL_ALLOC
  ACK: pavel
  - 5d0b56f6725d virtio_ring: fix return code on DMA mapping fails
  ACK: pavel
  - 78260a294c04 media: imon: invalid dereference in imon_touch_event
  ACK: pavel
  - 94a94b605916 media: cxusb: detect cxusb_ctrl_msg error in query
  ACK: pavel
  - 8b42c263ec1a media: b2c2-flexcop-usb: add sanity checking
  ACK: pavel
  - 56be9f1b8733 media: uvcvideo: Fix error path in control parsing failure
  UR: pavel -- is that correct? it is copying _different code_.
  - 61e73cf57ed8 cpufreq: Add NULL checks to show() and store() methods of cpufreq
  ACK: pavel
  - f217cef919da media: usbvision: Fix races among open, close, and disconnect
  UR: pavel
  - 467052f6ea5a media: vivid: Fix wrong locking that causes race conditions on streaming stop
  UR: pavel
  - b73b28b1b2cb media: vivid: Set vid_cap_streaming and vid_out_streaming to true
  UR: pavel
  - af8071f50f46 nfc: port100: handle command failure cleanly
  ACK: pavel
  - 3510fb7947d5 ALSA: usb-audio: Fix NULL dereference at parsing BADD
  ACK: pavel
  - 2819f4030f43 futex: Prevent robust futex exit race
  UR: pavel
  - d3f8c58d701c y2038: futex: Move compat implementation into futex.c
  UR: pavel
  - 344966da99c9 nbd: prevent memory leak
  ACK: pavel
  - ed7a3dde0aa2 x86/speculation: Fix redundant MDS mitigation message
  ACK: pavel -- just a printk fix
  - 0af5ae268e24 x86/speculation: Fix incorrect MDS/TAA mitigation status
  ACK: pavel
  - ed7312096a1f x86/insn: Fix awk regexp warnings
  ACK: pavel -- just warnings
  - 99b933bbc7b0 ARC: perf: Accommodate big-endian CPU
  ACK: pavel
  - e02f1448282b ARM: 8904/1: skip nomap memblocks while finding the lowmem/highmem boundary
  ACK: pavel
  - 046f0fcf7397 ocfs2: remove ocfs2_is_o2cb_active()
  UR: pavel
  - 36bef080b55f net: phy: dp83867: increase SGMII autoneg timer duration
  ACK: pavel
  - 87997a7800a1 net: phy: dp83867: fix speed 10 in sgmii mode
  ACK: pavel
  - 5779cbc98369 mm/memory_hotplug: don't access uninitialized memmaps in shrink_zone_span()
  UR: pavel -- not in inbox?!
  - a268d985f089 md/raid10: prevent access of uninitialized resync_pages offset
  ACK: pavel
  - f8dc0350d32b ath9k_hw: fix uninitialized variable data
  ACK: pavel
  - f0cfe98332d6 ath10k: Fix a NULL-ptr-deref bug in ath10k_usb_alloc_urb_from_pipe
  ACK: pavel
  - 4ae7392ab6f4 KVM: MMU: Do not treat ZONE_DEVICE pages as being reserved
  UR: pavel
  - 03bf4876a593 Bluetooth: Fix invalid-free in bcsp_close()
  ACK: pavel
  - 006360ec33d9 mm/page_io.c: do not free shared swap slots
  ACK: pavel
  - 16a300fb1dbb cfg80211: call disconnect_wk when AP stops
  ACK: pavel
  - 2b3541ffdd05 ipv6: Fix handling of LLA with VRF and sockets bound to VRF
  ACK: pavel
  - 091ed093c9c8 mm/memory_hotplug: Do not unlock when fails to take the device_hotplug_lock
  ACK: pavel
  - 896f7398152b i2c: uniphier-f: fix timeout error after reading 8 bytes
  ACK: pavel
  - 1efa17ab9cb8 spi: omap2-mcspi: Fix DMA and FIFO event trigger size mismatch
  ACK: pavel
  - 1b0f1b2dde06 nvme-pci: fix surprise removal
  ACK: pavel
  - 597a37d01b69 PCI: keystone: Use quirk to limit MRRS for K2G
  ACK: pavel
  - c0418c4a61aa pinctrl: zynq: Use define directive for PIN_CONFIG_IO_STANDARD
  ACK: pavel -- just a warning fix
  - 0858006cda38 pinctrl: lpc18xx: Use define directive for PIN_CONFIG_GPIO_PIN_INT
  ACK: pavel -- just a warning fix
  - 5efa36e7f04a pinctrl: bcm2835: Use define directive for BCM2835_PINCONF_PARAM_PULL
  ACK: pavel -- just a warning fix
  - bad4da12b797 pinctrl: qcom: spmi-gpio: fix gpio-hog related boot issues
  ACK: pavel
  - c24fe7807320 cfg80211: Prevent regulatory restore during STA disconnect in concurrent interfaces
  ACK: pavel
  - ee7d2473812b tools: bpftool: pass an argument to silence open_obj_pinned()
  - 367e64ce11fc of: unittest: initialize args before calling of_*parse_*()
  - e4547e028547 of: unittest: allow base devicetree to have symbol metadata
  - 1303c938c953 net: bcmgenet: return correct value 'ret' from bcmgenet_power_down
  ACK: pavel
  - 1d6a0dd6aa53 ACPICA: Use %d for signed int print formatting instead of %u
  ACK: pavel -- just a warning fix
  - d15b8b691725 clk: tegra20: Turn EMC clock gate into divider
  UR: pavel
  - 91c5f99d131e vrf: mark skb for multicast or link-local as enslaved to VRF
  UR: pavel
  - 5c2a3997ae5b dlm: don't leak kernel pointer to userspace
  ACK: pavel
  - afb4717ab81b dlm: fix invalid free
  ACK: pavel
  - 30fc13ae88b2 usb: typec: tcpm: charge current handling for sink during hard reset
  ACK: pavel -- optimalization, not a bugfix
  - 5e989b6cad68 scsi: lpfc: Correct loss of fc4 type on remote port address change
  - a391709b636e scsi: lpfc: Fix odd recovery in duplicate FLOGIs in point-to-point
  - 05678af01a33 scsi: lpfc: fcoe: Fix link down issue after 1000+ link bounces
  - e6d0262e1628 scsi: megaraid_sas: Fix goto labels in error handling
  - 83e679606e78 scsi: megaraid_sas: Fix msleep granularity
  - ff997bf13c1e scsi: mpt3sas: Fix driver modifying persistent data in Manufacturing page11
  - 80224e3b08ff scsi: mpt3sas: Don't modify EEDPTagMode field setting on SAS3.5 HBA devices
  - 5553e2bdcb02 scsi: mpt3sas: Fix Sync cache command failure during driver unload
  - 5f2dc610eb28 net: dsa: bcm_sf2: Turn on PHY to allow successful registration
  NAK: pavel -- misses cleanup in the error case
  - 2ac94b9e902f rtlwifi: rtl8192de: Fix misleading REG_MCUFWDL information
  ACK: pavel -- printk fix
  - 13d434fed1a2 wireless: airo: potential buffer overflow in sprintf()
  ACK: pavel
  - fad934bb2e47 brcmsmac: never log "tid x is not agg'able" by default
  ACK: pavel -- just a printk fix
  - bdb61fa50b70 rtl8xxxu: Fix missing break in switch
  ACK: pavel
  - 06ed77066838 wlcore: Fix the return value in case of error in 'wlcore_vendor_cmd_smart_config_start()'
  ACK: pavel
  - c68a499448e1 ath10k: snoc: fix unbalanced clock error handling
  ACK: pavel
  - c262dc0655df wil6210: fix locking in wmi_call
  ACK: pavel
  - ccb2390e8933 wil6210: fix RGF_CAF_ICR address for Talyn-MB
  UR: pavel -- formating is "interesting"
  - 0e1304ae42c2 wil6210: fix L2 RX status handling
  ACK: pavel
  - aa2112d57152 wil6210: fix debugfs memory access alignment
  ACK: pavel
  - ea136e61e4df btrfs: avoid link error with CONFIG_NO_AUTO_INLINE
  UR: pavel
  - 4a035f2643c6 media: ov13858: Check for possible null pointer
  pavel -- would it be better to fail initialization instead if allocations fail?
  - 3ee72f30c163 nds32: Fix bug in bitfield.h
  ACK: pavel
  - e6c6c0439a93 net: bpfilter: fix iptables failure if bpfilter_umh is disabled
  ACK: pavel
  - 811c8141d663 sock_diag: fix autoloading of the raw_diag module
  ACK: pavel
  - 3c69a033b416 audit: print empty EXECVE args
  ACK: pavel -- interface change, not clear bugfix
  - 5dc441fbef91 soc: bcm: brcmstb: Fix re-entry point with a THUMB2_KERNEL
  ACK: pavel
  - 9cdfff0a5e61 clk: sunxi-ng: enable so-said LDOs for A64 SoC's pll-mipi clock
  ACK: pavel
  - a2e6fe1f647f ARM: dts: imx6sx-sdb: Fix enet phy regulator
  ACK: pavel
  - 465073e44fda openvswitch: fix linking without CONFIG_NF_CONNTRACK_LABELS
  ACK: pavel
  - 31bced01fe77 sched/fair: Don't increase sd->balance_interval on newidle balance
  ACK: pavel
  - ed023646c2cf sched/topology: Fix off by one bug
  ACK: pavel
  - f9fca78e6cf2 net: do not abort bulk send on BQL status
  ACK: pavel -- optimalization, not a bugfix
  - 0d3b9ac2844f ocfs2: fix clusters leak in ocfs2_defrag_extent()
  - 6c150df9c2e8 ocfs2: don't put and assigning null to bh allocated outside
  - 6fd469d06eb8 ocfs2: don't use iocb when EIOCBQUEUED returns
  - abc7968b86b1 ocfs2: without quota support, avoid calling quota recovery
  - 6a2245d828e4 mm: handle no memcg case in memcg_kmem_charge() properly
  ACK: pavel
  - 08f07d9f5b17 tools/power turbosat: fix AMD APIC-id output
  ACK: pavel -- rewrite to support amd hardware, not a bugfix
  - b167fee4e45d arm64: makefile fix build of .i file in external module case
  ACK: pavel
  - fa7f1bce27c0 nvme-pci: fix conflicting p2p resource adds
  ACK: pavel -- just a WARN fix
  - 0bbb8382db4c irq/matrix: Fix memory overallocation
  ACK: pavel -- memory overallocation, not a serious bug
  - ad8298fccd70 ntb: intel: fix return value for ndev_vec_mask()
  ACK: pavel
  - 3bd4422af874 ntb_netdev: fix sleep time mismatch
  ACK: pavel -- just a performance fix
  - 8dbecf6c2f17 net: hns3: bugfix for hclge_mdio_write and hclge_mdio_read
  ACK: pavel
  - 12eeb72afac2 net: hns3: bugfix for is_valid_csq_clean_head()
  pavel -- is it right that test for >= desc_num disappeared?
  - e3345108409c net: hns3: bugfix for reporting unknown vector0 interrupt repeatly problem
  ACK: pavel
  - b0465d06d4c6 net: hns3: bugfix for buffer not free problem during resetting
  NAK: pavel -- introduces use-after-free: cda69d244585bc4497d3bb878c22fe2b6ad647c1
  - 3d9bc014c519 fm10k: ensure completer aborts are marked as non-fatal after a resume
  ACK: pavel -- not a minimum fix
  - 2fed73906e97 igb: shorten maximum PHC timecounter update interval
  ACK: pavel -- not a complete fix; will still break under high system loads
  - 3081ae5e378f powerpc/powernv: hold device_hotplug_lock when calling device_online()
  - 17523d7a1cc8 mm/memory_hotplug: fix online/offline_pages called w.o. mem_hotplug_lock
  ACK: pavel
  - 02735d5987b9 mm/memory_hotplug: make add_memory() take the device_hotplug_lock
  ACK: pavel -- buggy, fixed by next patch
  - 023c071f101c kernel/panic.c: do not append newline to the stack protector panic string
  ACK: pavel -- minor printk fix
  - 1914e5edd811 fs/hfs/extent.c: fix array out of bounds read of array extent
  - a9f38975eb98 hfs: update timestamp on truncate()
  - 0013adceb521 hfsplus: update timestamps on truncate()
  - 38e7b916daa2 hfs: fix return value of hfs_get_block()
  - 550da9ee35fd hfsplus: fix return value of hfsplus_get_block()
  - 8687d57d247a hfs: prevent btree data loss on ENOSPC
  - 0b54b59d8541 hfsplus: prevent btree data loss on ENOSPC
  - 7beaf6105e2e hfs: fix BUG on bnode parent update
  - 1df96949ebfb hfsplus: fix BUG on bnode parent update
  - 08751e477f0e lib/bitmap.c: fix remaining space computation in bitmap_print_to_pagebuf
  ACK: pavel -- fix for theoretical bug
  - 1d5834945945 linux/bitmap.h: fix type of nbits in bitmap_shift_right()
  ACK: pavel -- cleanup
  - 8deaaf77ce2e linux/bitmap.h: handle constant zero-size bitmaps correctly
  ACK: pavel -- robustness, does not fix known bug
  - 30598425aecc mm/gup_benchmark.c: prevent integer overflow in ioctl
  ACK: pavel
  - 9663d294ae28 block: call rq_qos_exit() after queue is frozen
  ACK: pavel
  - a125df22d134 selftests/powerpc/cache_shape: Fix out-of-tree build
  - 024cd793bbe1 selftests/powerpc/switch_endian: Fix out-of-tree build
  - a4a660f7abd9 selftests/powerpc/signal: Fix out-of-tree build
  - f74f406bbd0b selftests/powerpc/ptrace: Fix out-of-tree build
  - 57aab8f0a33d powerpc/xmon: Relax frame size for clang
  NAK: pavel
  - 32d7474b7a08 ipv4/igmp: fix v1/v2 switchback timeout based on rfc3376, 8.12
  ACK: pavel
  - 691bd94c15b1 vfs: avoid problematic remapping requests into partial EOF block
  ACK: pavel
  - cdc45f204775 um: Make line/tty semantics use true write IRQ
  ACK: pavel
  - a17e3bbfb97c i2c: uniphier-f: fix race condition when IRQ is cleared
  ACK: pavel
  - a118403a5ecc i2c: uniphier-f: fix occasional timeout error
  ACK: pavel
  - 1466eae37a9f i2c: uniphier-f: make driver robust against concurrency
  ACK: pavel
  - 10807b374663 block: fix the DISCARD request merge
  ACK: pavel
  - b948d56951be macsec: let the administrator set UP state even if lowerdev is down
  - f5bdad7106fc macsec: update operstate when lower device changes
  - 4291e97c69f6 mm: thp: fix MADV_DONTNEED vs migrate_misplaced_transhuge_page race condition
  ACK: pavel
  - ac1cad79bcad tools/testing/selftests/vm/gup_benchmark.c: fix 'write' flag usage
  - 2d9d6c099eaf mm/page-writeback.c: fix range_cyclic writeback vs writepages deadlock
  ACK: pavel
  - bcba80f38a00 fs/ocfs2/dlm/dlmdebug.c: fix a sleep-in-atomic-context bug in dlm_print_one_mle()
  - 99b3146b799a arm64: lib: use C string functions with KASAN enabled
  ACK: pavel -- kasan improvement, not a bugfix
  - b84e965c7edb sparc64: Rework xchg() definition to avoid warnings.
  IGN: pavel
  - 4e4cad4365e0 powerpc/process: Fix flush_all_to_thread for SPE
  - 54299e1cf379 bpf, btf: fix a missing check bug in btf_parse
  - 8044e741eee4 bpf: devmap: fix wrong interface selection in notifier_call
  - 7b557dbdc519 net: ethernet: cadence: fix socket buffer corruption problem
  ACK: pavel
  - 3681b901e94a thermal: rcar_thermal: Prevent hardware access during system suspend
  ACK: pavel
  - 436e610bf195 thermal: rcar_thermal: fix duplicate IRQ request
  ACK: pavel
  - 51aa1a10fbac selftests: fix warning: "_GNU_SOURCE" redefined
  - c62be4108883 selftests: kvm: Fix -Wformat warnings
  - 5802cb25de86 selftests: watchdog: Fix error message.
  - 74685702363c selftests: watchdog: fix message when /dev/watchdog open fails
  - 58ceffabad41 selftests/ftrace: Fix to test kprobe $comm arg only if available
  - adcb6d9ff121 spi: uniphier: fix incorrect property items
  ACK: pavel -- just a documentation fix
  - 6c2075f792c6 fs/cifs: fix uninitialised variable warnings
  ACK: pavel -- just a warning fix
  - c5621fbd6560 net: socionext: Stop PHY before resetting netsec
  ACK: pavel
  - 0a6fa6119ab1 mfd: max8997: Enale irq-wakeup unconditionally
  ACK: pavel -- behaviour change, not a bugfix
  - 5a04242e8c23 mfd: intel_soc_pmic_bxtwc: Chain power button IRQs as well
  - dd72391c9a53 mfd: mc13xxx-core: Fix PMIC shutdown when reading ADC values
  ACK: pavel
  - 02c9ec11860f mfd: arizona: Correct calling of runtime_put_sync
  ACK: pavel
  - e79d230832d0 net: ethernet: ti: cpsw: unsync mcast entries while switch promisc mode
  ACK: pavel
  - 7c011435110f qlcnic: fix a return in qlcnic_dcb_get_capability()
  ACK: pavel
  - 321c40dce581 mISDN: Fix type of switch control variable in ctrl_teimanager
  IGN: pavel
  - 074af1668038 f2fs: spread f2fs_set_inode_flags()
  ACK: pavel
  - e7f81efae3b5 f2fs: fix to spread clear_cold_data()
  ACK: pavel
  - 733fddc34651 thermal: armada: fix a test in probe()
  ACK: pavel
  - bb925b9bece2 RISC-V: Avoid corrupting the upper 32-bit of phys_addr_t in ioremap
  ACK: pavel
  - 829aa617bf6e rtc: s35390a: Change buf's type to u8 in s35390a_init
  ACK: pavel -- just a warning
  - 8112f3d4ed03 ceph: only allow punch hole mode in fallocate
  - 146fb4b0d4fe ceph: fix dentry leak in ceph_readdir_prepopulate
  - 083757d84859 tools: bpftool: fix completion for "bpftool map update"
  ACK: pavel
  - 570c05378d97 selftests/bpf: fix return value comparison for tests in test_libbpf.sh
  ACK: pavel -- it might be better to just make it #!/bin/bash
  - 3173e226ca41 powerpc/64s/radix: Fix radix__flush_tlb_collapsed_pmd double flushing pmd
  - b43c5287f6bc powerpc/mm/radix: Fix small page at boundary when splitting
  - b499fa070ee4 powerpc/mm/radix: Fix overuse of small pages in splitting logic
  - 434551e9fd44 powerpc/mm/radix: Fix off-by-one in split mapping logic
  - ee35e01b0f08 powerpc/pseries: Export raw per-CPU VPA data via debugfs
  - 9ed143cf730d scsi: hisi_sas: Fix NULL pointer dereference
  ACK: pavel
  - ff6618e06cb4 sparc: Fix parport build warnings.
  ACK: pavel -- just a warning fix
  - 3d02e3bb3cfe x86/intel_rdt: Prevent pseudo-locking from using stale pointers
  ACK: pavel
  - b6e44f743951 spi: omap2-mcspi: Set FIFO DMA trigger level to word length
  ACK: pavel -- not a minimum bugfix
  - ad9a4e963c92 swiotlb: do not panic on mapping failures
  NAK: pavel -- seems like good way to 
  - 9b572e8bc038 s390/perf: Return error when debug_register fails
  IGN: pavel
  - 641f1f798c80 atm: zatm: Fix empty body Clang warnings
  ACK: pavel
  - f9304c6277ed sunrpc: safely reallow resvport min/max inversion
  ACK: pavel -- changes strange interface, not a bugfix
  - 7983dea8c368 SUNRPC: Fix a compile warning for cmpxchg64()
  ACK: pavel -- just a warning fix
  - a0ec7f6eabe8 selftests/bpf: fix file resource leak in load_kallsyms
  ACK: pavel -- tiny leak in userspace .. is it even a bug?
  - 56b8b1832122 dm raid: avoid bitmap with raid4/5/6 journal device
  ACK: pavel
  - 4de506d51177 sctp: use sk_wmem_queued to check for writable space
  ACK: pavel
  - 1f7f2a0666a3 usbip: tools: fix atoi() on non-null terminated string
  ACK: pavel
  - 283d9618e22b USB: misc: appledisplay: fix backlight update_status return code
  ACK: pavel
  - 80a23f70d546 PCI: vmd: Detach resources after stopping root bus
  ACK: pavel -- tiny memory leak
  - b0f69ccfff3a macintosh/windfarm_smu_sat: Fix debug output
  ACK: pavel -- refactors debugging support, not a bugfix
  - 86f63146f886 ALSA: i2c/cs8427: Fix int to char conversion
  ACK: pavel -- just a warning fix
  - 46729b27706a PM / Domains: Deal with multiple states but no governor in genpd
  ACK: pavel -- robustness, not a bugfix
  - cf800f2b630b ACPI / scan: Create platform device for INT33FE ACPI nodes
  ACK: pavel -- turns driver type from i2c to platform..
  - cb6a3096ec2f kprobes, x86/ptrace.h: Make regs_get_kernel_stack_nth() not fault on bad stack
  ACK: pavel -- just robustness, does not fix known bug
  - f0f842a1a9f9 xfs: clear ail delwri queued bufs on unmount of shutdown fs
  - bb64349b6f56 xfs: fix use-after-free race in xfs_buf_rele
  - e0e8d83e4878 net: ena: Fix Kconfig dependency on X86
  ACK: pavel
  - 7ac437558e4a net: fix warning in af_unix
  ACK: pavel -- just a warning fix
  - 5e110ec2d924 net: dsa: mv88e6xxx: Fix 88E6141/6341 2500mbps SERDES speed
  ACK: pavel
  - 274726bcf5ba scsi: zorro_esp: Limit DMA transfers to 65535 bytes
  - 1f13afca914f scsi: dc395x: fix DMA API usage in sg_update_list
  - e95ec662b126 scsi: dc395x: fix dma API usage in srb_done
  - 95655b10529d ASoC: tegra_sgtl5000: fix device_node refcounting
  ACK: pavel
  - f1f1002a9987 clk: at91: audio-pll: fix audio pmc type
  ACK: pavel
  - f15b802890bb clk: mmp2: fix the clock id for sdh2_clk and sdh3_clk
  ACK: pavel
  - 6391dd5eb0ef PCI: mediatek: Fixup MSI enablement logic by enabling MSI before clocks
  ACK: pavel -- not a minimal fix
  - 305c262fb8ee nvme-pci: fix hot removal during error handling
  ACK: pavel
  - 4e4b97f59050 nvmet-fcloop: suppress a compiler warning
  ACK: pavel -- just a warning fix
  - 2f1e4e65ba41 nvmet: avoid integer overflow in the discard code
  ACK: pavel
  - 30ca1af49481 crypto: ccree - avoid implicit enum conversion
  ACK: pavel
  - 5bbeb4fcf39a scsi: iscsi_tcp: Explicitly cast param in iscsi_sw_tcp_host_get_param
  ACK: pavel -- just a warning fix
  - 06815ae5e27f scsi: bfa: Avoid implicit enum conversion in bfad_im_post_vendor_event
  ACK: pavel -- just a warning fix
  - f4bf2dc77826 scsi: isci: Change sci_controller_start_task's return type to sci_status
  ACK: pavel -- just a warning fix
  - 3a40068dc831 scsi: isci: Use proper enumerated type in atapi_d2h_reg_frame_handler
  ACK: pavel -- just a warning fix
  - 6288f52ed258 clk: tegra: Fixes for MBIST work around
  ACK: pavel
  - 5a487f40e717 KVM/x86: Fix invvpid and invept register operand size in 64-bit mode
  ACK: pavel
  - 7392aa08f8a4 KVM: nVMX: move check_vmentry_postreqs() call to nested_vmx_enter_non_root_mode()
  - 9fe573d539a8 KVM: nVMX: reset cache/shadows when switching loaded VMCS
  - cec14148c1a9 nfp: bpf: protect against mis-initializing atomic counters
  ACK: pavel -- not sure this fixes serious bug
  - bfe01cddb81a scsi: ips: fix missing break in switch
  ACK: pavel
  - 073f454cc28c qed: Align local and global PTT to propagate through the APIs.
  ACK: pavel -- not sure what bug it fixes
  - 45f89cf0f329 amiflop: clean up on errors during setup
  IGN: pavel
  - 4043bc0f634b pwm: lpss: Only set update bit if we are actually changing the settings
  - ad78a958663a pinctrl: sunxi: Fix a memory leak in 'sunxi_pinctrl_build_state()'
  ACK: pavel
  - fae3cf8874c3 RDMA/bnxt_re: Avoid resource leak in case the NQ registration fails
  - b1bf1e424dcb RDMA/bnxt_re: Fix qp async event reporting
  - 2f241e333f7b RDMA/bnxt_re: Avoid NULL check after accessing the pointer
  - bbeeadb7bab4 scsi: hisi_sas: Free slot later in slot_complete_vx_hw()
  ACK: pavel
  - 628cae51693a scsi: hisi_sas: Fix the race between IO completion and timeout for SMP/internal IO
  ACK: pavel
  - d10b7dd14a37 scsi: hisi_sas: Feed back linkrate(max/min) when re-attached
  ACK: pavel
  - 92ac0e324974 m68k: fix command-line parsing when passed from u-boot
  ACK: pavel
  - 4ab1594e699e w1: IAD Register is yet readable trough iad sys file. Fix snprintf (%u for unsigned, count for max size).
  ACK: pavel -- new driver features, not a bugfix
  - d11d985d0a82 misc: mic: fix a DMA pool free failure
  ACK: pavel
  - a2204a295554 gsmi: Fix bug in append_to_eventlog sysfs handler
  ACK: pavel
  - cc08097f2619 btrfs: handle error of get_old_root
  ACK: pavel
  - 1b49c453a564 btrfs: defrag: use btrfs_mod_outstanding_extents in cluster_pages_for_defrag
  ACK: pavel
  - 9325e8f4688c PCI: mediatek: Fix class type for MT7622 to PCI_CLASS_BRIDGE_PCI
  ACK: pavel
  - dfffae314a02 mmc: mediatek: fix cannot receive new request when msdc_cmd_is_ready fail
  ACK: pavel
  - e2c158f58522 mmc: mediatek: fill the actual clock for mmc debugfs
  UR: pavel -- cleanup, not a bugfix
  - e3fa491211c4 spi: sh-msiof: fix deferred probing
  ACK: pavel
  - 2cd0b70c55b0 cdrom: don't attempt to fiddle with cdo->capability
  ACK: pavel
  - 0c5c34c1aaf3 skd: fixup usage of legacy IO API
  ACK: pavel
  - 196b007ac0a6 ath10k: allocate small size dma memory in ath10k_pci_diag_write_mem
  ACK: pavel
  - 17fbe3c91ad4 ath10k: set probe request oui during driver start
  ACK: pavel
  - 8111f99f7c44 brcmsmac: AP mode: update beacon when TIM changes
  ACK: pavel
  - 20e42ddf6c1d mt76x0: phy: fix restore phase in mt76x0_phy_recalibrate_after_assoc
  ACK: pavel
  - 8cb568162524 mt76: do not store aggregation sequence number for null-data frames
  ACK: pavel
  - b95998fb6c50 EDAC, thunderx: Fix memory leak in thunderx_l2c_threaded_isr()
  ACK: pavel
  - 97aab1a43a2f powerpc/eeh: Fix use of EEH_PE_KEEP on wrong field
  - bd2a7e53cd6e powerpc/eeh: Fix null deref for devices removed during EEH
  - 16e4657a1d25 powerpc/boot: Disable vector instructions
  - 5346c8403733 powerpc/boot: Fix opal console in boot wrapper
  - 4505cff2ef4a powerpc: Fix signedness bug in update_flash_db()
  - 93b943c0602f synclink_gt(): fix compat_ioctl()
  pavel -- not a minimum fix; should we keep list of ioctl numbers?
  - 8d67a4ecb473 pty: fix compat ioctls
  ACK: pavel
  - fa3fe5f442ab gfs2: Fix marking bitmaps non-full
  ACK: pavel
  - 26a4c6a562ce PCI: cadence: Write MSI data with 32bits
  ACK: pavel -- not sure what the impact is
  - ca71f9c8adc2 pinctrl: madera: Fix uninitialized variable bug in madera_mux_set_mux
  ACK: pavel -- just a printk fix
  - 4465a916eaa7 printk: fix integer overflow in setup_log_buf()
  ACK: pavel -- just a printk fix
  - 90d73768ddb6 printk: lock/unlock console only for new logbuf entries
  ACK: pavel
  - 8888689bd433 crypto: testmgr - fix sizeof() on COMP_BUF_SIZE
  ACK: pavel
  - 3757657af27e ALSA: isight: fix leak of reference to firewire unit in error path of .probe callback
  UR: pavel -- ? also changes ordering 
  - 49a9643b5e8d mwifiex: Fix NL80211_TX_POWER_LIMITED
  ACK: pavel -- this has potential to cause problems; it limits power where regulations require it
  - e80e88ef6057 drm/i915/userptr: Try to acquire the page lock around set_page_dirty()
  ACK: pavel
  - a0ee03bb5269 drm/i915/pmu: "Frequency" is reported as accumulated cycles
  ACK: pavel -- interface change, not really serious bug
  - 8a67fbf65971 drm/amd/powerplay: issue no PPSMC_MSG_GetCurrPkgPwr on unsupported ASICs
  ACK: pavel -- just a printk fix
  - e8d355befc42 mm/ksm.c: don't WARN if page is still mapped in remove_stable_node()
  ACK: pavel
  - b28da9da6e51 Revert "fs: ocfs2: fix possible null-pointer dereferences in ocfs2_xa_prepare_entry()"
  UR: pavel
  - 67380639dfdb virtio_console: allocate inbufs in add_port() only if it is needed
  UR: pavel
  - 65d153c8ed65 nbd:fix memory leak in nbd_get_socket()
  ACK: pavel
  - 036588ec6888 tools: gpio: Correctly add make dependencies for gpio_utils
  ACK: pavel
  - 7cb8ee734c18 gpio: max77620: Fixup debounce delays
  ACK: pavel
  - 70d594d17ebb vhost/vsock: split packets to send using multiple buffers
  UR: pavel
  - 48bc34efbc65 net/mlx5: Fix auto group size calculation
  - 28a4cc2b5d6f net/mlxfw: Verify FSM error code translation doesn't exceed array size
  - 7c1a53817782 net/mlx5e: Fix set vf link state error flow
  - 1ff2a0f8692f sfc: Only cancel the PPS workqueue if it exists
  ACK: pavel
  - 13512a5eb818 net: sched: ensure opts_len <= IP_TUNNEL_OPTS_MAX in act_tunnel_key
  ACK: pavel
  - 2ba6a4f5402e net/sched: act_pedit: fix WARN() in the traffic path
  ACK: pavel
  - 9f6de5cf5390 net: rtnetlink: prevent underflows in do_setvfinfo()
  UR: pavel
  - ebcb0840a76b net/mlx4_en: Fix wrong limitation for number of TX rings
  - 5408138df1db net/mlx4_en: fix mlx4 ethtool -N insertion
  - baa888ca02db mlxsw: spectrum_router: Fix determining underlay for a GRE tunnel
