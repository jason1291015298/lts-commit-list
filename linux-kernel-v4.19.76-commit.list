# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.76
  - 555161e Linux 4.19.76
  - 59a5cea f2fs: use generic EFSBADCRC/EFSCORRUPTED
  - fc3d296 net/rds: Check laddr_check before calling it
  - 3de749d net/rds: An rds_sock is added too early to the hash table
  ACK: pavel
  - 07f7ec8 net_sched: check cops->tcf_block in tc_bind_tclass()
  ACK: pavel
  - 90b0761 Bluetooth: btrtl: Additional Realtek 8822CE Bluetooth devices
  ACK: pavel
  - 6934809 netfilter: nft_socket: fix erroneous socket assignment
  ACK: pavel
  - 649836f xfs: don't crash on null attr fork xfs_bmapi_read
  ACK: pavel
  - 91ae872 drm/nouveau/disp/nv50-: fix center/aspect-corrected scaling
  ACK: pavel
  - 3717f4a ACPI: video: Add new hw_changes_brightness quirk, set it on PB Easynote MZ35
  ACK: pavel
  - 46beb6e Bluetooth: btrtl: HCI reset on close for Realtek BT chip
  ACK: pavel
  - 8ffd7ba net: don't warn in inet diag when IPV6 is disabled
  ACK: pavel
  - ff0fbfa drm: Flush output polling on shutdown
  ACK: pavel
  - 303f6d6 f2fs: fix to do sanity check on segment bitmap of LFS curseg
  ACK: pavel -- would like to see -EUCLEAN
  - dec0955 net/ibmvnic: Fix missing { in __ibmvnic_reset
  ACK: pavel
  - dc9118f dm zoned: fix invalid memory access
  ACK: pavel
  - 73d90f5 Revert "f2fs: avoid out-of-range memory access"
  pavel -- not sure I like this. Does not fix a bug. Makes code less robust.
  - 40cdc71 blk-mq: move cancel of requeue_work to the front of blk_exit_queue
  ACK: pavel
  - 313efb2 blk-mq: change gfp flags to GFP_NOIO in blk_mq_realloc_hw_ctxs
  ACK: pavel -- but I wonder if this makes code more fragile
  - 75448f4 initramfs: don't free a non-existent initrd
  ACK: pavel
  - ad16dfe bcache: remove redundant LIST_HEAD(journal) from run_cache_set()
  ACK: pavel -- removal of reduntant code, does not fix a bug
  - 08fdaee PCI: hv: Avoid use of hv_pci_dev->pci_slot after freeing it
  ACK: pavel
  - ef2baa0 f2fs: check all the data segments against all node ones
  ACK: pavel
  - ef06c33 irqchip/gic-v3-its: Fix LPI release for Multi-MSI devices
  ACK: pavel
  - 52b4947 bpf: libbpf: retry loading program on EAGAIN
  ACK: pavel
  - a935d78 Revert "drm/amd/powerplay: Enable/Disable NBPSTATE on On/OFF of UVD"
  ACK: pavel
  - 288831c scsi: qla2xxx: Return switch command on a timeout
  ACK: pavel
  - 2b983f2 scsi: qla2xxx: Remove all rports if fabric scan retry fails
  ACK: pavel
  - 6b449e4 scsi: qla2xxx: Turn off IOCB timeout timer on IOCB completion
  ACK: pavel
  - 9423770 locking/lockdep: Add debug_locks check in __lock_downgrade()
  pavel -- two patches with same title but different content, Tetsuo solves this
  - 0c23335 power: supply: sysfs: ratelimit property read error message
  ACK: pavel -- just a printk fix
  - 1456c40 pinctrl: sprd: Use define directive for sprd_pinconf_params values
  ACK: pavel -- just a workaround for clang warning, ugly
  - 037d73a objtool: Clobber user CFLAGS variable
  ACK: pavel
  - 952844f ALSA: hda - Apply AMD controller workaround for Raven platform
  ACK: pavel
  - 50c9ccf ALSA: hda - Add laptop imic fixup for ASUS M9V laptop
  ACK: pavel
  - 866a1a7 ALSA: dice: fix wrong packet parameter for Alesis iO26
  ACK: pavel
  - 9829fd2 ALSA: usb-audio: Add DSD support for EVGA NU Audio
  ACK: pavel
  - 3039212 ALSA: usb-audio: Add Hiby device family to quirks for native DSD support
  ACK: pavel
  - 8a845c0 ASoC: fsl: Fix of-node refcount unbalance in fsl_ssi_probe_from_dt()
  ACK: pavel -- just a tiny memory leak
  - a5e2c65 ASoC: Intel: cht_bsw_max98090_ti: Enable codec clock once and keep it enabled
  ACK: pavel
  - ec2a368 media: tvp5150: fix switch exit in set control handler
  ACK: pavel
  - ba68607 iwlwifi: mvm: always init rs_fw with 20MHz bandwidth rates
  ACK: pavel
  - ced0676 iwlwifi: mvm: send BCAST management frames to the right station
  ACK: pavel -- just "looks fishy", not serious bug
  - b3873e3 net/mlx5e: Rx, Check ip headers sanity
  ACK: pavel
  - 404f118 net/mlx5e: Rx, Fixup skb checksum for packets with tail padding
  ACK: pavel -- performance optimalization, not a bugfix; not quite trivial
  - c95ebb3 net/mlx5e: XDP, Avoid checksum complete when XDP prog is loaded
  ACK: pavel
  - 79e972a net/mlx5e: Allow reporting of checksum unnecessary
  ACK: pavel -- adds testing infrastructure; not everything is required for subsequent patches
  - 8da68f7 mlx5: fix get_ip_proto()
  ACK: pavel
  - 44da025 net/mlx5e: don't set CHECKSUM_COMPLETE on SCTP packets
  ACK: pavel -- buggy, fixed by the next patch
  - 6debda97 net/mlx5e: Set ECN for received packets using CQE indication
  ACK: pavel
  - e867ef1 CIFS: fix deadlock in cached root handling
  ACK: pavel
  - f3160a1 crypto: talitos - fix missing break in switch statement
  ACK: pavel
  - c1a7fe4 mtd: cfi_cmdset_0002: Use chip_good() to retry in do_write_oneword()
  ACK: pavel
  - 5fdefdc HID: Add quirk for HP X500 PIXART OEM mouse
  ACK: pavel
  - 3d072c2 HID: hidraw: Fix invalid read in hidraw_ioctl
  ACK: pavel
  - acc96be HID: logitech: Fix general protection fault caused by Logitech driver
  ACK: pavel
  - 3e78517 HID: sony: Fix memory corruption issue on cleanup.
  ACK: pavel
  - eb77929 HID: prodikeys: Fix general protection fault during probe
  ACK: pavel
  - 2661d46 IB/core: Add an unbound WQ type to the new CQ API
  ACK: pavel
  - 70ec2ee drm/amd/display: readd -msse2 to prevent Clang from emitting libcalls to undefined SW FP routines
  ACK: pavel -- just a clang compile fix
  - 80fc279 powerpc/xive: Fix bogus error code returned by OPAL
  IGN: pavel
  - 4eb92a1 RDMA/restrack: Protect from reentry to resource return path
  ACK: pavel
  - 373f909 net/ibmvnic: free reset work of removed device from queue
  ACK: pavel
  - 2af977b Revert "Bluetooth: validate BLE connection interval updates"
  ACK: pavel
