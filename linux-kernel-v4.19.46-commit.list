# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.46
  - 8b2fc0058255 Linux 4.19.46
  - fcac71697a15 fbdev: sm712fb: fix memory frequency by avoiding a switch/case fallthrough
  - 107e215c2962 bpf, lru: avoid messing with eviction heuristics upon syscall lookup
  ACK: pavel
  - 2bb3c5470aaf bpf: add map_lookup_elem_sys_only for lookups from syscall side
  pavel -- just adds unused infrastructure
  - 3ded3aaa4aa8 bpf: relax inode permission check for retrieving bpf program
  pavel -- loosens permission check. Not sure if 'bug' is serious enough.
  - c33563e9ec87 Revert "selftests/bpf: skip verifier tests for unsupported program types"
  ACK: pavel
  - 90110ffd86ae driver core: Postpone DMA tear-down until after devres release for probe failure
  ACK: pavel
  - 430908054540 md/raid: raid5 preserve the writeback action after the parity check
  pavel --I don't think I understand consequences here
  - 3d25b7f5c3be Revert "Don't jump to compute_result state from check_result state"
  pavel -- changelog would be nice
  - a0b1dde1e686 perf/x86/intel: Fix race in intel_pmu_disable_event()
  ACK: pavel
  - 7aea2f94cc64 perf bench numa: Add define for RUSAGE_THREAD if not present
  ACK: pavel
  - a06fdd99a339 ufs: fix braino in ufs_get_inode_gid() for solaris UFS flavour
  ACK: pavel
  - f037116fe05b x86/mm/mem_encrypt: Disable all instrumentation for early SME setup
  ACK: pavel -- But this is pretty ugly.
  - 290da8e79c83 sched/cpufreq: Fix kobject memleak
  ACK: pavel
  - 2da19da7abb8 iwlwifi: mvm: check for length correctness in iwl_mvm_create_skb()
  ACK: pavel
  - 7341daa0548e qmi_wwan: new Wistron, ZTE and D-Link devices
  ACK: pavel
  - c1528193f643 bpf: Fix preempt_enable_no_resched() abuse
  ACK: pavel
  - aea54f613534 power: supply: sysfs: prevent endless uevent loop with CONFIG_POWER_SUPPLY_DEBUG
  ACK: pavel
  - a1251522a522 KVM: arm/arm64: Ensure vcpu target is unset on reset failure
  ACK: pavel
  - 36ae546a0046 net: ieee802154: fix missing checks for regmap_update_bits
  ACK: pavel -- but not sure if bug is not theoretical
  - 9c045d8c9739 mac80211: Fix kernel panic due to use of txq after free
  ACK: pavel
  - a0a49d8712de x86: kvm: hyper-v: deal with buggy TLB flush requests from WS2012
  pavel -- workaround for Windows 2012 bug.
  - a469646862aa PCI: Fix issue with "pci=disable_acs_redir" parameter being ignored
  ACK: pavel
  - b21ca2769b0f apparmorfs: fix use-after-free on symlink traversal
  ACK: pavel
  - 9a0467e1f671 securityfs: fix use-after-free on symlink traversal
  ACK: pavel
  - 900bf351dd84 power: supply: cpcap-battery: Fix division by zero
  ACK: pavel
  - b7771cb0143b clk: sunxi-ng: nkmp: Avoid GENMASK(-1, 0)
  ACK: pavel
  - a654a73de29f xfrm4: Fix uninitialized memory read in _decode_session4
  ACK: pavel
  - 6faa62060624 xfrm: Honor original L3 slave device in xfrmi policy lookup
  ACK: pavel
  - 3716c2625099 esp4: add length check for UDP encapsulation
  ACK: pavel
  - d410ef75886a xfrm: clean up xfrm protocol checks
  ACK: pavel -- not sure if bug it fixes is serious enough. It says cleanup...
  - 159269cc6456 vti4: ipip tunnel deregistration fixes.
  ACK: pavel
  - 64f214ce563f xfrm6_tunnel: Fix potential panic when unloading xfrm6_tunnel module
  ACK: pavel
  - c9516503fe53 xfrm: policy: Fix out-of-bound array accesses in __xfrm_policy_unlink
  ACK: pavel
  - fea685000caf fuse: Add FOPEN_STREAM to use stream_open()
  pavel -- adds new interface for userspace; not exactly a bugfix. Strange hole in constantts
  - f9eccf6ca1e0 dm mpath: always free attached_handler_name in parse_path()
  ACK: pavel -- But the code is tricky
  - 9407680a7bb7 dm integrity: correctly calculate the size of metadata area
  ACK: pavel
  - 3b92ff729cb3 dm delay: fix a crash when invalid device is specified
  ACK: pavel
  - 90cc71127a3c dm zoned: Fix zone report handling
  ACK: pavel
  - ff0699a5e5d0 dm cache metadata: Fix loading discard bitset
  ACK: pavel
  - d5c352305d42 PCI: Work around Pericom PCIe-to-PCI bridge Retrain Link erratum
  ACK: pavel
  - b51a033317cd PCI: Factor out pcie_retrain_link() function
  pavel -- preparation for next patch
  - 7bc992e215c8 PCI: rcar: Add the initialization of PCIe link in resume_noirq()
  ACK: pavel
  - 2e7574982502 PCI/AER: Change pci_aer_init() stub to return void
  ACK: pavel
  - 8c30e1499335 PCI: Init PCIe feature bits for managed host bridge alloc
  - 29d031402718 PCI: Mark Atheros AR9462 to avoid bus reset
  ACK: pavel
  - f4be6b7ee294 PCI: Mark AMD Stoney Radeon R7 GPU ATS as broken
  ACK: pavel
  - 2cf1dce1bfa5 fbdev: sm712fb: fix crashes and garbled display during DPMS modesetting
  pavel -- series of patches to support old thinkpad
  - 27968d821368 fbdev: sm712fb: use 1024x768 by default on non-MIPS, fix garbled display
  pavel -- series of patches to support old thinkpad
  - f1c97f633375 fbdev: sm712fb: fix support for 1024x768-16 mode
  pavel -- series of patches to support old thinkpad
  - b415308ae49a fbdev: sm712fb: fix crashes during framebuffer writes by correctly mapping VRAM
  ACK: pavel -- dunno. That old thinkpad needs a bit too many "fixes"
  - 02f89dd99c83 fbdev: sm712fb: fix boot screen glitch when sm712fb replaces VGA
  pavel -- boot screen glitch does not seem serious
  - 7e1b9659a43a fbdev: sm712fb: fix white screen of death on reboot, don't set CR3B-CR3F
  pavel -- series of patches to support old thinkpad
  - b0f08070903d fbdev: sm712fb: fix VRAM detection, don't set SR70/71/74/75
  pavel -- series of patches to support old thinkpad
  - d30768975973 fbdev: sm712fb: fix brightness control on reboot, don't set SR30
  pavel -- series of patches to support old thinkpad
  - 702156cd1a9a fbdev/efifb: Ignore framebuffer memmap entries that lack any memory types
  ACK: pavel
  - e738fb38cf2e objtool: Allow AR to be overridden with HOSTAR
  pavel -- enables build with llvm. Not sure if that counts...
  - 9ae0f86ceaa7 MIPS: perf: Fix build with CONFIG_CPU_BMIPS5000 enabled
  ACK: pavel -- but not exactly minimal fix
  - 05fab3457210 perf intel-pt: Fix sample timestamp wrt non-taken branches
  ACK: pavel
  - ba86f8f84fd5 perf intel-pt: Fix improved sample timestamp
  ACK: pavel
  - 3ed850ab2a9c perf intel-pt: Fix instructions sampling rate
  ACK: pavel
  - 5e011f3319fe memory: tegra: Fix integer overflow on tick value calculation
  ACK: pavel -- fixes reduced performace, but fix is quite simple/obvious
  - fb8c9c900d4e tracing: Fix partial reading of trace event's id file
  ACK: pavel
  - 07b487eb5762 ftrace/x86_64: Emulate call function while updating in breakpoint handler
  ACK: pavel
  - ba246f64b0a5 x86_64: Allow breakpoints to emulate call instructions
  pavel -- preparation for...?
  - 01b6fdcecd5a x86_64: Add gap to int3 to allow for call emulation
  pavel -- preparation for 53/?
  - 77ca91441696 ceph: flush dirty inodes before proceeding with remount
  ACK: pavel
  - b18339bc1d05 iommu/tegra-smmu: Fix invalid ASID bits on Tegra30/114
  ACK: pavel
  - a9676c96e7e0 ovl: fix missing upper fs freeze protection on copy up for ioctl
  ACK: pavel
  - 979d2433b873 fuse: honor RLIMIT_FSIZE in fuse_file_fallocate
  ACK: pavel -- same area is modified by subsequent patches; new version does not just return
  - a452f733f93e fuse: fix writepages on 32bit
  ACK: pavel
  - 42f59b83f0cf udlfb: introduce a rendering mutex
  ACK: pavel
  - fb36a97654a7 udlfb: fix sleeping inside spinlock
  ACK: pavel
  - 1b8c955691d4 udlfb: delete the unused parameter for dlfb_handle_damage
  pavel --just a cleanup
  - 3487804cf6dc clk: rockchip: fix wrong clock definitions for rk3328
  ACK: pavel
  - fe082b99d57b clk: mediatek: Disable tuner_en before change PLL rate
  ACK: pavel
  - 5bfba9529cea clk: tegra: Fix PLLM programming on Tegra124+ when PMC overrides divider
  ACK: pavel
  - 1a7adc2edb98 clk: hi3660: Mark clk_gate_ufs_subsys as critical
  ACK: pavel
  - 04f34b76368f PNFS fallback to MDS if no deviceid found
  ACK: pavel
  - d3dd6057d2d6 NFS4: Fix v4.0 client state corruption when mount
  ACK: pavel
  - 5e7f9e905ff8 media: imx: Clear fwnode link struct for each endpoint iteration
  ACK: pavel
  - ef12f5b54da4 media: imx: csi: Allow unknown nearest upstream entities
  ACK: pavel
  - 77e178708136 media: ov6650: Fix sensor possibly not detected on probe
  - 86d67dbdf0a0 phy: ti-pipe3: fix missing bit-wise or operator when assigning val
  ACK: pavel
  - 939db6fdbea6 cifs: fix strcat buffer overflow and reduce raciness in smb21_set_oplock_level()
  ACK: pavel
  - a29b8829291e of: fix clang -Wunsequenced for be32_to_cpu()
  ACK: pavel
  - a36430769ee5 p54: drop device reference count if fails to enable device
  ACK: pavel
  - 88cfd822f9d0 intel_th: msu: Fix single mode with IOMMU
  ACK: pavel
  - c939121b5435 dcache: sort the freeing-without-RCU-delay mess for good.
  ACK: pavel
  - 10cb519c3e34 md: add mddev->pers to avoid potential NULL pointer dereference
  ACK: pavel
  - 3deaa1dc2f70 md: batch flush requests.
  pavel -- (significant) performance improvement. Is it okay to use time for global ordering like that?
  - 7f6b9285cada Revert "MD: fix lock contention for flush bios"
  ACK: pavel
  - 7928396df91e proc: prevent changes to overridden credentials
  ACK: pavel
  - bbd559ad3ca7 brd: re-enable __GFP_HIGHMEM in brd_insert_page()
  - d9ec75d048d7 stm class: Fix channel bitmap on 32-bit systems
  ACK: pavel
  - 44bc4e8815a4 stm class: Fix channel free in stm output free path
  - 85b94de88046 parisc: Rename LEVEL to PA_ASM_LEVEL to avoid name clash with DRBD code
  ACK: pavel
  - e5621f7e13f6 parisc: Use PA_ASM_LEVEL in boot code
  - 615260c947b4 parisc: Skip registering LED when running in QEMU
  - 9aabffe8c2a6 parisc: Export running_on_qemu symbol for modules
  - b11efd3262ef net/mlx5e: Fix ethtool rxfh commands when CONFIG_MLX5_EN_RXNFC is disabled
  - 79742133aff2 net/mlx5: Imply MLXFW in mlx5_core
  - 9f12f4c922d4 vsock/virtio: Initialize core virtio vsock before registering the driver
  ACK: pavel
  - 4b900077784f tipc: fix modprobe tipc failed after switch order of device registration
  ACK: pavel
  - 4af8a327aeba vsock/virtio: free packets during the socket release
  ACK: pavel
  - 2f7025b0a3b3 tipc: switch order of device registration to fix a crash
  - 2636da604e76 rtnetlink: always put IFLA_LINK for links with a link-netnsid
  ACK: pavel
  - c73ed24c385a ppp: deflate: Fix possible crash in deflate_init
  ACK: pavel
  - e4a6df16b441 nfp: flower: add rcu locks when accessing netdev for tunnels
  - 948cd616504c net: usb: qmi_wwan: add Telit 0x1260 and 0x1261 compositions
  ACK: pavel
  - 3620e546b177 net: test nouarg before dereferencing zerocopy pointers
  ACK: pavel
  - 0495c8b03545 net/mlx4_core: Change the error print to info print
  - 746f8cd570ba net: avoid weird emergency message
  ACK: pavel -- but just fixes a message
  - 466cadba6013 net: Always descend into dsa/
  pavel -- why is it needed?
  - 6bc3240adde5 ipv6: prevent possible fib6 leaks
  - 81a61a95812e ipv6: fix src addr routing with the exception table
