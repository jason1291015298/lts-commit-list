# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.45
  - c3a072597748 Linux 4.19.45
  - e8816d3bc595 ext4: don't update s_rev_level if not required
  ACK: pavel -- I guess supporting 20 year old filesystems is really important...
  - 6172ae55a187 ext4: fix compile error when using BUFFER_TRACE
  ACK: pavel
  - 953e826e8d0f pstore: Refactor compression initialization
  pavel -- not sure what it fixes.
  - fea8b84765a1 pstore: Allocate compression during late_initcall()
  pavel -- makes pstore more useful, but not really bugfix.
  - f4bf101be366 pstore: Centralize init/exit routines
  pavel -- not sure what bug it fixes?
  - 627bb2d93b4d iov_iter: optimize page_copy_sane()
  pavel -- performance optimalization
  - 866f011181ff libnvdimm/namespace: Fix label tracking error
  - 756eda9bc8b7 xen/pvh: set xen_domain_type to HVM in xen_pvh_init
  ACK: pavel
  - 98bdd33883db kbuild: turn auto.conf.cmd into a mandatory include file
  ACK: pavel
  - 38f114887ca4 KVM: lapic: Busy wait for timer to expire when using hv_timer
  ACK: pavel
  - 3b5ea2df6cf6 KVM: x86: Skip EFER vs. guest CPUID checks for host-initiated writes
  NAK: pavel -- 0 vs. false confusion, not sure what bug it fixes.
  - 5b8567682489 jbd2: fix potential double free
  pavel -- not even close to minimal fix
  - 95482af27161 ALSA: hda/realtek - Fix for Lenovo B50-70 inverted internal microphone bug
  ACK: pavel
  - e0e1dc65bb13 ALSA: hda/realtek - Fixup headphone noise via runtime suspend
  pavel -- does not sound like a minimal fix... that would be one liner.
  - ae3155123704 ALSA: hda/realtek - Corrected fixup for System76 Gazelle (gaze14)
  ACK: pavel
  - 316063bf7d11 ext4: avoid panic during forced reboot due to aborted journal
  ACK: pavel
  - c19db366c0a8 ext4: fix use-after-free in dx_release()
  ACK: pavel
  - 0db24122bd7f ext4: fix data corruption caused by overlapping unaligned and aligned IO
  ACK: pavel
  - 25d010f4e0ec ext4: zero out the unused memory region in the extent tree block
  ACK: pavel
  - c907ce3fd552 tty: Don't force RISCV SBI console as preferred console
  ACK: pavel -- behaviour change in console handling
  - 986d3453bee4 fs/writeback.c: use rcu_barrier() to wait for inflight wb switches going into workqueue when umount
  ACK: pavel
  - a80da82d0840 crypto: ccm - fix incompatibility between "ccm" and "ccm_base"
  pavel -- not exactly sure what bug...
  - f6de0a3b1e66 ipmi:ssif: compare block number correctly for multi-part return messages
  ACK: pavel
  - 88681649ed8c bcache: never set KEY_PTRS of journal key to 0 in journal_reclaim()
  ACK: pavel
  - ecfc882f6441 bcache: fix a race between cache register and cacheset unregister
  ACK: pavel
  - 8a8f671b3dad Btrfs: do not start a transaction at iterate_extent_inodes()
  ACK: pavel
  - 0388d45afc50 Btrfs: do not start a transaction during fiemap
  ACK: pavel
  - 74ca0a7671cc Btrfs: send, flush dellaloc in order to avoid data loss
  ACK: pavel
  - 8b13bb911f0c btrfs: Honour FITRIM range constraints during free space trim
  ACK: pavel
  - 87dcf0c61985 btrfs: Correctly free extent buffer in case btree_read_extent_buffer_pages fails
  ACK: pavel
  - d8925a1fee71 btrfs: Check the first key and level for cached extent buffer
  ACK: pavel
  - 45123ae534e0 ext4: fix ext4_show_options for file systems w/o journal
  ACK: pavel
  - f795247578aa ext4: actually request zeroing of inode table after grow
  ACK: pavel
  - 2a18c9c76718 ext4: fix use-after-free race with debug_want_extra_isize
  ACK: pavel
  - b12a8d80a46e ext4: avoid drop reference to iloc.bh twice
  ACK: pavel
  - f0f805f8b9e7 ext4: ignore e_value_offs for xattrs with value-in-ea-inode
  ACK: pavel
  - 71478ef67d7c ext4: make sanity check in mballoc more strict
  ACK: pavel
  - 001fe0dab4ea jbd2: check superblock mapped prior to committing
  ACK: pavel
  - 0fd2df64f142 tty/vt: fix write/write race in ioctl(KDSKBSENT) handler
  ACK: pavel -- that's some seriously ugly code
  - d90824ecb887 tty: vt.c: Fix TIOCL_BLANKSCREEN console blanking if blankinterval == 0
  ACK: pavel
  - 6a01793e0763 mtd: spi-nor: intel-spi: Avoid crossing 4K address boundary on read/write
  ACK: pavel
  - dc6d69bde829 mfd: max77620: Fix swapped FPS_PERIOD_MAX_US values
  ACK: pavel
  - 5185672f2acf mfd: da9063: Fix OTP control register names to match datasheets for DA9063/63L
  NAK: pavel -- is it just changing headers for userspace benefit?
  - 770e46b38ebe ACPI: PM: Set enable_for_wake for wakeup GPEs during suspend-to-idle
  ACK: pavel
  - 8bae43985571 userfaultfd: use RCU to free the task struct when fork fails
  ACK: pavel
  - 3574bc98e2fe ocfs2: fix ocfs2 read inode data panic in ocfs2_iget
  ACK: pavel
  - a3ccc156f365 hugetlb: use same fault hash key for shared and private mappings
  ACK: pavel
  - 0b16b09a723e mm/hugetlb.c: don't put_page in lock of hugetlb_lock
  ACK: pavel
  - 58db3813680e mm/huge_memory: fix vmf_insert_pfn_{pmd, pud}() crash, handle unaligned addresses
  ACK: pavel -- but minimal fix would be way smaller
  - f580a54bbd52 mm/mincore.c: make mincore() more conservative
  pavel -- should we return error when we deny permissions? Should we return 0 if we lie?
  - 681f3695d514 crypto: ccree - handle tee fips error during power management resume
  ACK: pavel -- additional error checking, would not be surprised if it broke someone's config
  - 4fb3d87ee7b7 crypto: ccree - add function to handle cryptocell tee fips error
  pavel -- just a cleanup, preparation for the next patch
  - 65f5c14a6011 crypto: ccree - HOST_POWER_DOWN_EN should be the last CC access during suspend
  ACK: pavel
  - 1a4fc3d29632 crypto: ccree - pm resume first enable the source clk
  ACK: pavel
  - 120ab825c6fd crypto: ccree - don't map AEAD key and IV on stack
  ACK: pavel
  - ca687cdb6159 crypto: ccree - use correct internal state sizes for export
  ACK: pavel
  - 766121a0a798 crypto: ccree - don't map MAC key on stack
  ACK: pavel -- but I believe variable could become local
  - 7560c0adad34 crypto: ccree - fix mem leak on error path
  pavel -- insignificant memory leak
  - 642de1c00a14 crypto: ccree - remove special handling of chained sg
  pavel -- cleanup, not a serious bug fix
  - 1bfceb375034 bpf, arm64: remove prefetch insn in xadd mapping
  ACK: pavel
  - f3714257c422 ASoC: codec: hdac_hdmi add device_link to card device
  ACK: pavel
  - 975ef5c2f6ca ASoC: fsl_esai: Fix missing break in switch statement
  ACK: pavel -- would like to know what user-visible bug is
  - df9f111db871 ASoC: RT5677-SPI: Disable 16Bit SPI Transfers
  ACK: pavel
  - 7295359bd6ac ASoC: max98090: Fix restore of DAPM Muxes
  ACK: pavel
  - e13bac4031eb ALSA: hdea/realtek - Headset fixup for System76 Gazelle (gaze14)
  NAK: pavel -- this patch is known broken
  - d33f6063b7c3 ALSA: hda/realtek - EAPD turn on later
  pavel -- reduction of clicking noises, not really bugfix for serious bug
  - 4ac6316a7c0f ALSA: hda/hdmi - Consider eld_valid when reporting jack event
  ACK: pavel
  - 8c827cda2864 ALSA: hda/hdmi - Read the pin sense from register when repolling
  ACK: pavel
  - 30dda277333e ALSA: usb-audio: Fix a memory leak bug
  ACK: pavel
  - 741e3efd8174 ALSA: line6: toneport: Fix broken usage of timer for delayed execution
  ACK: pavel
  - 003cf675eb07 mmc: core: Fix tag set memory leak
  ACK: pavel
  - d42d342022b1 crypto: arm64/aes-neonbs - don't access already-freed walk.iv
  ACK: pavel
  - 69b9d32d5139 crypto: arm/aes-neonbs - don't access already-freed walk.iv
  ACK: pavel
  - b7d2adfd0512 crypto: rockchip - update IV buffer to contain the next IV
  ACK: pavel
  - 9a61ab689867 crypto: gcm - fix incompatibility between "gcm" and "gcm_base"
  pavel -- changes algorithm names
  - 63efe31cf544 crypto: arm64/gcm-aes-ce - fix no-NEON fallback code
  ACK: pavel
  - e7fd8a2862e0 crypto: x86/crct10dif-pcl - fix use via crypto_shash_digest()
  ACK: pavel
  - 7a19a4bef218 crypto: crct10dif-generic - fix use via crypto_shash_digest()
  ACK: pavel
  - aabf86f24d9f crypto: skcipher - don't WARN on unprocessed data after slow walk step
  ACK: pavel -- just fixes a WARN_ON()
  - 66f5de68cb61 crypto: vmx - fix copy-paste error in CTR mode
  ACK: pavel
  - 07d677ae4db4 crypto: ccp - Do not free psp_master when PLATFORM_INIT fails
  ACK: pavel -- enables chip use after init error. Hopefully it does not uncover any bugs.
  - fe632ee5ade8 crypto: chacha20poly1305 - set cra_name correctly
  ACK: pavel
  - 3b5ddd5ea016 crypto: salsa20 - don't access already-freed walk.iv
  ACK: pavel -- theoretical bug, may not be triggerable in 4.19
  - 7a32ad34b889 crypto: crypto4xx - fix cfb and ofb "overran dst buffer" issues
  ACK: pavel -- it has cleanup as a bonus, typo subtile -> subtle
  - c1ec6beac625 crypto: crypto4xx - fix ctr-aes missing output IV
  ACK: pavel
  - 2ea1a37d0138 sched/x86: Save [ER]FLAGS on context switch
  ACK: pavel
  - d8d751efec28 arm64: Save and restore OSDLR_EL1 across suspend/resume
  ACK: pavel
  - f273cd16554a arm64: Clear OSDLR_EL1 on CPU boot
  ACK: pavel
  - 26e7d2ad97b9 arm64: compat: Reduce address limit
  ACK: pavel
  - 6d696ceb15a3 arm64: arch_timer: Ensure counter register reads occur with seqlock held
  ACK: pavel
  - 222abad906ba arm64: mmap: Ensure file offset is treated as unsigned
  ACK: pavel
  - 592127e9c1bb power: supply: axp288_fuel_gauge: Add ACEPC T8 and T11 mini PCs to the blacklist
  ACK: pavel
  - 26eb5e7fa08d power: supply: axp288_charger: Fix unchecked return value
  ACK: pavel
  - 921bc15462e2 ARM: exynos: Fix a leaked reference by adding missing of_node_put
  ACK: pavel
  - 6eaeee1e7845 mmc: sdhci-of-arasan: Add DTS property to disable DCMDs.
  ACK: pavel
  - e2c436d9268f ARM: dts: exynos: Fix audio (microphone) routing on Odroid XU3
  ACK: pavel
  - abea1fb53266 ARM: dts: exynos: Fix interrupt for shared EINTs on Exynos5260
  ACK: pavel
  - 8cf1bbca4467 arm64: dts: rockchip: Disable DCMDs on RK3399's eMMC controller.
  ACK: pavel -- needs another patch to be effective
  - 7b72ca6312ab objtool: Fix function fallthrough detection
  pavel -- just changes build warning into better one
  - b185029f5c41 x86/speculation/mds: Improve CPU buffer clear documentation
  ACK: pavel -- docs update, no code change
  - 393ca9ea37fb x86/speculation/mds: Revert CPU buffer clear on double fault exit
  pavel -- not sure what the bug is
  - 7761dbf58d22 locking/rwsem: Prevent decrement of reader count before increment
  ACK: pavel
