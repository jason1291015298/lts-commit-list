#!/usr/bin/python3
#
# Various tools useful for working with and reviewing commits
#
# - can parse commit lists from git history or linux-kernel-*-commit.list files.
# - can parse commit to learn referenced upstream commit.
# - can search for already-reviewed commits based on upstream commit.
# - work in progress, more work needed :-).
# 
# GPLv2+
# -*- python -*-

import os
import re
import sys
import relevant

class Commit:
    def __init__(m, hash):
        m.hash = hash
        m.upstream = None
        m.summary = None
        m.state = None
        m.release = None
        m.comment = None
        m.fixes = None
        m.fixes_status = ":"
        m.relevant = ":"

    def parse_commit(m):
        part = 0
        #print("Parsing commit", m.hash)
        for l in os.popen("cd ../krc; git cat-file -p %s" % m.hash).readlines():
            l = l[:-1]
            #print(part, l)
            if part == 0:
                if l == "":
                    part = 1
                    continue

            if part == 1:
                if not m.summary:
                    m.summary = l
                ma = re.match(".*Upstream commit ([0-9a-f]*) .*", l)
                if ma:
                    m.upstream = ma.group(1)
                ma = re.match("[Cc]ommit ([0-9a-f]*) upstream[.]*", l)
                if ma:
                    m.upstream = ma.group(1)

                ma = re.match("Fixes: ([0-9a-f]*).*", l)
                if ma:
                    m.fixes = ma.group(1)
                    
                if re.match("^Signed-off-by:", l) \
                   or re.match("^Reported-by:", l) \
                   or re.match("^Fixes:", l) \
                   or False:
                    part = 2
                    continue

        if part < 2:
            #print("Parsing of commit", m.hash, "failed")
            pass
                
    def print_summary(m):
        upstream = None
        if m.upstream:
            upstream = m.upstream[:12]
        print(" | %s %s %s%s | %s" % (m.hash[:12], upstream, m.relevant, m.fixes_status, m.summary))

class List:
    def __init__(m):
        m.commits = []
        
    def list_from_git(m, num):
        for l in os.popen("cd ../krc; git log --pretty=oneline | head -%d" % num).readlines():
            s = l.split(" ")
            hash = s[0]
            summary = l[41:-1]
            c = Commit(hash)
            c.summary = summary
            m.commits += [ c ]

    def print(m):
        for c in m.commits:
            c.print_summary()
            
    def list_from_list_file(m, f, release = None):
        hash = None
        m.cur = None
        for l in f.readlines():
            l = l[:-1]
            r = re.match("^  - ([0-9a-f]*) (.*)$", l)
            if r:
                hash = r.group(1)
                m.cur = Commit(hash)
                m.cur.release = release
                m.cur.summary = r.group(2)
                m.commits += [ m.cur ]
                #m.cur.parse_commit()
                continue
            if l[:6] == "  ACK:":
                #print("Have ack for ", hash)
                m.cur.state = "a"
                m.cur.comment = l[6:]
            if l[:6] == "  NAK:":
                #print("Have nak for ", hash)
                m.cur.state = "n"
                m.cur.comment = l[6:]
            if l[:7] == "  pavel":
                #print("Have comment for ", hash)
                m.cur.state = "?"
                m.cur.comment = l[7:]

            #print(l)

    def list_from_summary(m, f):
        hash = None
        m.cur = None
        for l in f.readlines():
            l = l[:-1]
            s = l.split(" | ")
            if len(s) < 3:
                print("Could not parse: ", l)
                continue
            c = Commit("00000000")
            if len(s[0]) > 0:
                c.state = s[0][0]
                c.comment = s[0][1:]
            c.summary = s[2]
            m.commits += [ c ]

    def write_list_file(m, f, version = "v4.19.XXX"):
        f.write("""# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## %s
""" % version)
        for c in m.commits:
            f.write("  - "+c.hash[:12]+" "+c.summary+"\n")
            if c.state:
                s = ""
                if c.state == "?":
                    s = "  pavel"
                elif c.state == "a":
                    s = "  ACK: pavel"
                elif c.state == "n":
                    s = "  NAK: pavel"
                elif c.state == "i":
                    s = "  IGN: pavel"
                else: s = "  UR: pavel"
                if c.comment:
                    s = s + " --" + c.comment
                s += "\n"
                f.write(s)

    def print_count(m, count, total, s):
        print("  ", count[s], "(%.0f%%)" % (100*count[s]/total), s)

    def statistics(m):
        total = 0
        count = {}
        count["a"] = 0
        count["n"] = 0
        count["?"] = 0
        count["upstream"] = 0

        for c in m.commits:
            total += 1
            if c.state:
                count[c.state] += 1
            if c.upstream:
                count["upstream"] += 1
            else:
                print("Not upstream:", c.hash, c.summary)
        print(total, "commits total;")
        m.print_count(count, total, "a")
        m.print_count(count, total, "n")
        m.print_count(count, total, "?")
        m.print_count(count, total, "upstream")

    def list_from_all_files(m):
        for l in os.popen("ls -1 ../lts-commit-list/linux-kernel-v4.*-commit.list").readlines():
            l = l[:-1]
            print("File:", l)
            f = open(l, "r")
            m.list_from_list_file(f, l)

    def same_upstream(m, c, c2):
        if c.state == c2.state:
            return
        if c.state == None:
            d = c
            d2 = c2
        elif c2.state == None:
            d = c2
            d2 = c
        else:
            print("!!! Different state and neither is empty", c.hash, c2.hash)
            return
        print("*** Commit needs new state:", d.hash, d.release, d2.hash, d2.state, d2.release)

    def parse_all(m):
        for c in m.commits:
            c.parse_commit()

    def relevant_all(m):
        r = relevant.RelevantCommit()
        r.load_review()
        for c in m.commits:
            c.relevant = r.check_files(c.hash)

    def fixes_all(m):
        for c in m.commits:
            if not c.fixes:
                c.fixes_status = " "
                continue
            if not os.system("cd ../krc; git merge-base --is-ancestor HEAD %s" % c.fixes):
                c.fixes_status = "X"
            else:
                c.fixes_status = "+"

    def upstream(m):
        common = 0
        upstream_map = {}
        for c in m.commits:
            c.parse_commit()
            if c.upstream:
                if c.upstream in upstream_map:
                    c2 = upstream_map[c.upstream]
                    #print("Commit", c.hash, "is common in", c.hash, c2.hash)
                    m.same_upstream(c, c2)
                    common += 1
                else:
                    upstream_map[c.upstream] = c

        print(common, "commits have common upstream commit")

    def add_review(m, summary, review):
        num = 0
        matching = None
        for c in m.commits:
            s = c.summary[:len(summary)]
            #print(s, summary)                
            if s == summary:
                matching = c
                num = num+1
        if num == 0:
            print(summary, "### no match?")
            return
        if num > 1:
            print("?! ", num, "matching commits for ", summary)
            return
        matching.state = review[0]
        review = review[1:]
        if review != " ":
            matching.comment = review

    def add_reviews(m, lo):
        for c in lo.commits:
            if c.state:
                m.add_review(c.summary, c.state + c.comment)

    def add_adhoc_review(m, f):
        for l in f.readlines():
            l = l[:-1]
            #print(l)
            r = re.match("(.*)[0-9][0-9][0-9]/[0-9][0-9][0-9]\] (.*)", l)
            #print("review: ", r.group(1), "title: ", r.group(2))
            review  = r.group(1)
            summary = r.group(2)
            m.add_review(summary, review)

def rc_to_reviews(ver):
    l = List()
    l.list_from_list_file(open("linux-kernel-"+ver+"-commit.list", "r"))
    lo = List()
    lo.list_from_summary(open("rc-"+ver+".list", "r"))
    l.add_reviews(lo)
    l.write_list_file(open("delme.list", "w"), ver)

def full_rc_to_reviews(ver):
    rc_to_reviews(ver)
    os.system("mv delme.list linux-kernel-"+ver+"-commit.list")
    os.system("mv rc-"+ver+".list rc-"+ver+".list.done")

def git_to_rc(num):
    l = List()
    l.list_from_git(num)
    l.parse_all()
    l.relevant_all()
    #l.fixes_all()
    l.print()

def find_duplicities():
    l = List()
    l.list_from_all_files()
    l.upstream()
    #l.statistics()
    pass

def commandline(arg):
    if arg[1] == "-n":
        # Convert files from git to review template
        # Usage: -n NUMBER of patches
        i = int(arg[2])
        git_to_rc(i)
        return

    if arg[1] == "-d":
        find_duplicities()
        return

    # Add reviews from "rc" to final format.
    # Usage: v4.19.69
    full_rc_to_reviews(sys.argv[1])

if __name__ == "__main__":
    commandline(sys.argv)

