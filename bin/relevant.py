#!/usr/bin/python3
#
# Various tools useful for working with and reviewing commits
#
# GPLv2+
# -*- python -*-

import os
import re
import sys
import commit

class RelevantCommit:
    ignore_files = [
        "drivers/infiniband/",                
        "drivers/isdn/",
        "drivers/s390/",                                        
        "drivers/staging/",
        "drivers/parisc/",
        "arch/s390/",
        "arch/mips/",
        "tools/",
    ]

    ignore_files2 = [
        "arch/sparc/",
        "arch/um/",
        "arch/parisc/",
        "arch/openrisc/",
        "arch/m68k/",
        "arch/ia64/",
        "arch/alpha/",
        "drivers/staging/",
        "drivers/eisa/",
        "drivers/ide/",
        "drivers/parisc/",
        "drivers/pcmcia/",
        "drivers/macintosh/",                                        
        "drivers/vme/",
        "drivers/zorro/",
        "fs/reiserfs/",
        "fs/ntfs/",
        "fs/freevxfs/",
        "fs/gfs2/",
    ]

    def in_list(m, l, val):
        for i in l:
            if i == val[:len(i)]:
                return True
        return False

    def check_files(m, hash):
        part = 0
        #print("Parsing commit", hash)
        can_ignore = True
        important = False
        for l in os.popen("cd ../krc; git show %s | diffstat -l -p 1" % hash).readlines():
            l = l[:-1]
            # print(l)
            if not m.in_list(m.ignore_files, l):
                can_ignore = False
            if m.in_list(m.review_files, l):
            	important = True

        #print("Can be ignored?", can_ignore)
        r = " "
        if not can_ignore:
            r = "."
        if important:
            r = "o"
        if can_ignore and important:
            r = "X"
        return r

    def load_review(m):
        m.review_files = []
        for l in os.popen("cat ../cip-kernel-config/4.19.y-cip/all.sources").readlines():
            l = l[:-1]
            m.review_files += [ l ]

    def get_counts(m):
        sum = 0
        for i in m.ignore_files:
            c = os.popen("find /data/l/k/%s -name '*.[ch]' | xargs cat | wc -l" % i).readline()
            c = int(c)
            print(c, i)
            sum += c
        all = 24056427
        print("Total ", sum, 100.*sum/all, "%")

if __name__ == "__main__":
    r = RelevantCommit()
    r.load_review()
    print(r.check_files("ef5ed9dae24351cea5722d94a6c99e8b88b56b39"))
    #r.get_counts()
    #r.load_review()
