# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.116
  - 8488c3f3bc86 Linux 4.19.116
  - 94fe9e44dc69 efi/x86: Fix the deletion of variables in mixed mode
  - 502d8e56139c mfd: dln2: Fix sanity checking for endpoints
  - 04fe2fbdc00b etnaviv: perfmon: fix total and idle HI cyleces readout
  ACK: pavel -- support for different hardware, very poor changelog
  - bbff44d60243 misc: echo: Remove unnecessary parentheses and simplify check for zero
  ACK: pavel -- just a warning fix
  - 87279a3576de powerpc/fsl_booke: Avoid creating duplicate tlb1 entry
  ACK: pavel -- performance optimalization, not a bugfix
  - 52f1c4257c5a ftrace/kprobe: Show the maxactive number on kprobe_events
  ACK: pavel -- behaviour change, not  bugfix
  - 49d7fa0eb234 drm: Remove PageReserved manipulation from drm_pci_alloc
  ACK: pavel -- not sure if it fixes uses-visible bug
  - a0522bbd37d8 drm/dp_mst: Fix clearing payload state on topology disable
  ACK: pavel
  - 9a61fe235c0a Revert "drm/dp_mst: Remove VCPI while disabling topology mgr"
  ACK: pavel
  - 1e0c380514ef crypto: ccree - only try to map auth tag if needed
  ACK: pavel
  - dd5f4a04077b crypto: ccree - dec auth tag size from cryptlen map
  ACK: pavel
  - a44ed6982270 crypto: ccree - don't mangle the request assoclen
  ACK: pavel
  - c8cff87dc21f crypto: ccree - zero out internal struct before use
  ACK: pavel
  - c08805aae181 crypto: ccree - improve error handling
  ACK: pavel -- error handling improvement, not a bugfix
  - 9b463a246df9 crypto: caam - update xts sector size for large input length
  ACK: pavel
  - 738d853b1696 dm zoned: remove duplicate nr_rnd_zones increase in dmz_init_zone()
  ACK: pavel
  - 45b9d993dfe6 btrfs: use nofs allocations for running delayed items
  ACK: pavel
  - 473575d518b1 powerpc: Make setjmp/longjmp signature standard
  pavel -- different workaround for kernel warning
  - 303b647b8e74 powerpc: Add attributes for setjmp/longjmp
  ACK: pavel -- does not change code generation
  - 3851b89b88e4 scsi: mpt3sas: Fix kernel panic observed on soft HBA unplug
  ACK: pavel
  - 82efee6a87c1 powerpc/kprobes: Ignore traps that happened in real mode
  ACK: pavel
  - d1de05f20c92 powerpc/xive: Use XIVE_BAD_IRQ instead of zero to catch non configured IPIs
  ACK: pavel
  - 7ab2eb7ce7be powerpc/hash64/devmap: Use H_PAGE_THP_HUGE when setting up huge devmap PTE entries
  UR: pavel -- BUG then return?
  - c14e3ade0126 powerpc/64/tm: Don't let userspace set regs->trap via sigreturn
  ACK: pavel
  - 1855c5436fa5 powerpc/powernv/idle: Restore AMR/UAMOR/AMOR after idle
  UR: pavel -- preparation for something?
  - 5f547e7cbd84 xen/blkfront: fix memory allocation flags in blkfront_setup_indirect()
  ACK: pavel
  - 0721b549d353 ipmi: fix hung processes in __get_guid()
  ACK: pavel
  - 7676f94f041e libata: Return correct status in sata_pmp_eh_recover_pm() when ATA_DFLAG_DETACH is set
  ACK: pavel
  - 3151839ff4f6 hfsplus: fix crash and filesystem corruption when deleting files
  ACK: pavel
  - 35e68aef6539 cpufreq: powernv: Fix use-after-free
  ACK: pavel
  - 2a87b491b7df kmod: make request_module() return an error when autoloading is disabled
  ACK: pavel -- just a warning fix
  - ae6baa8ceca0 clk: ingenic/jz4770: Exit with error if CGU init failed
  ACK: pavel
  - cdfa83e14d72 Input: i8042 - add Acer Aspire 5738z to nomux list
  ACK: pavel
  - e68129e68163 s390/diag: fix display of diagnose call statistics
  - d1b6feb46bc6 perf tools: Support Python 3.8+ in Makefile
  - 13380f2b41b1 ocfs2: no need try to truncate file beyond i_size
  ACK: pavel
  - d4b3709cc7b6 fs/filesystems.c: downgrade user-reachable WARN_ONCE() to pr_warn_once()
  ACK: pavel -- just a printk tweak
  - 803ef6fa586d ext4: fix a data race at inode->i_blocks
  ACK: pavel -- just a theoretical bug
  - 194a805daefa NFS: Fix a page leak in nfs_destroy_unlinked_subrequests()
  ACK: pavel
  - 83dc8f0a9134 powerpc/pseries: Avoid NULL pointer dereference when drmem is unavailable
  ACK: pavel
  - 9c6c45935ce6 drm/etnaviv: rework perfmon query infrastructure
  ACK: pavel -- not sure how serious bug this fixes
  - 23599f816c56 rtc: omap: Use define directive for PIN_CONFIG_ACTIVE_HIGH
  ACK: pavel -- just a warning fix
  - 01522e4d4ad9 selftests: vm: drop dependencies on page flags from mlock2 tests
  - fb3e9f475712 arm64: armv8_deprecated: Fix undef_hook mask for thumb setend
  ACK: pavel
  - af77e3e4112a scsi: zfcp: fix missing erp_lock in port recovery trigger for point-to-point
  - 92760667471a dm verity fec: fix memory leak in verity_fec_dtr
  ACK: pavel
  - 6f3a303a34f5 dm writecache: add cond_resched to avoid CPU hangs
  ACK: pavel
  - a6d77a5ce056 arm64: dts: allwinner: h6: Fix PMU compatible
  ACK: pavel -- not sure it causes real problems
  - 0389387ea9cc net: qualcomm: rmnet: Allow configuration updates to existing devices
  pavel -- new feature, not a bugfix
  - 695986163d66 mm: Use fixed constant in page_frag_alloc instead of size + 1
  UR: pavel -- just a tiny optimalization, not a bugfix
  - 2e22edcd7351 tools: gpio: Fix out-of-tree build regression
  - 6209e0981bc4 x86/speculation: Remove redundant arch_smt_update() invocation
  ACK: pavel -- just a performance fix
  - f5e2eef0f636 powerpc/pseries: Drop pointless static qualifier in vpa_debugfs_init()
  ACK: pavel -- cleanup, not a bugfix
  - d8bd8bca10a2 erofs: correct the remaining shrink objects
  ACK: pavel
  - c127f180ece2 crypto: mxs-dcp - fix scatterlist linearization for hash
  ACK: pavel
  - ed8703409b6f btrfs: fix missing semaphore unlock in btrfs_sync_file
  ACK: pavel
  - 867ae5eb0ae8 btrfs: fix missing file extent item for hole after ranged fsync
  ACK: pavel
  - d8ecdce1549a btrfs: drop block from cache on error in relocation
  ACK: pavel
  - d3a7c4b8d950 btrfs: set update the uuid generation as soon as possible
  ACK: pavel -- just a performance fix
  - 7ed0c4db49d0 Btrfs: fix crash during unmount due to race with delayed inode workers
  ACK: pavel
  - d389050b45d4 mtd: spinand: Do not erase the block before writing a bad block marker
  ACK: pavel
  - a8899631d6b9 mtd: spinand: Stop using spinand->oobbuf for buffering bad block markers
  ACK: pavel
  - 9bc022589575 CIFS: Fix bug which the return value by asynchronous read is error
  ACK: pavel
  - f9971a898a81 KVM: VMX: fix crash cleanup when KVM wasn't used
  ACK: pavel
  - 4538f42a825c KVM: x86: Gracefully handle __vmalloc() failure during VM allocation
  ACK: pavel
  - a9f890aa8dca KVM: VMX: Always VMCLEAR in-use VMCSes during crash with kexec support
  ACK: pavel
  - 4a0efabb90b6 KVM: x86: Allocate new rmap and large page tracking when moving memslot
  ACK: pavel
  - de2ac8a719fd KVM: s390: vsie: Fix delivery of addressing exceptions
  - 50a59d2df794 KVM: s390: vsie: Fix region 1 ASCE sanity shadow address checks
  - deecbb365568 KVM: nVMX: Properly handle userspace interrupt window request
  ACK: pavel
  - 7460d17c2a49 x86/entry/32: Add missing ASM_CLAC to general_protection entry
  ACK: pavel
  - a2a1be2de7e4 signal: Extend exec_id to 64bits
  ACK: pavel -- not a complete fix for 32bit
  - 19e119d4b455 ath9k: Handle txpower changes even when TPC is disabled
  ACK: pavel
  - cde7e6605017 MIPS: OCTEON: irq: Fix potential NULL pointer dereference
  - 67dea3c78e6e MIPS/tlbex: Fix LDDIR usage in setup_pw() for Loongson-3
  - 76b48e986967 pstore: pstore_ftrace_seq_next should increase position index
  ACK: pavel
  - 977cab66547f irqchip/versatile-fpga: Apply clear-mask earlier
  ACK: pavel
  - 14b963594404 KEYS: reaching the keys quotas correctly
  ACK: pavel -- allows reaching maximum capacity, not a serious bug
  - 6415769223b0 tpm: tpm2_bios_measurements_next should increase position index
  ACK: pavel
  - 1da36bedeab0 tpm: tpm1_bios_measurements_next should increase position index
  ACK: pavel
  - 7c775e8e6cce tpm: Don't make log failures fatal
  UR: pavel -- should we still return errors but ignore them?
  - 9ffaeee7bce4 PCI: endpoint: Fix for concurrent memory allocation in OB address region
  ACK: pavel
  - d2345d1231d8 PCI: Add boot interrupt quirk mechanism for Xeon chipsets
  UR: pavel -- is that reasonable way to code that?
  - a73afecb4178 PCI/ASPM: Clear the correct bits when enabling L1 substates
  ACK: pavel
  - 1ada617e3627 PCI: pciehp: Fix indefinite wait on sysfs requests
  ACK: pavel
  - 011529b7d90b nvme: Treat discovery subsystems as unique subsystems
  ACK: pavel
  - 287ea8b4bd84 nvme-fc: Revert "add module to ops template to allow module references"
  ACK: pavel
  - 46cc8837482c thermal: devfreq_cooling: inline all stubs for CONFIG_DEVFREQ_THERMAL=n
  ACK: pavel
  - d56a8ea400a7 acpi/x86: ignore unspecified bit positions in the ACPI global lock field
  ACK: pavel
  - 811a3f83f718 media: ti-vpe: cal: fix disable_irqs to only the intended target
  ACK: pavel
  - 2c3dab1b74e8 ALSA: hda/realtek - Add quirk for MSI GL63
  ACK: pavel
  - e71c369b6471 ALSA: hda/realtek - Remove now-unnecessary XPS 13 headphone noise fixups
  ACK: pavel -- removes unneccessary quirks, not a bugfix
  - 92b27256fcc2 ALSA: hda/realtek - Set principled PC Beep configuration for ALC256
  ACK: pavel
  - 7cb3c1987ac1 ALSA: doc: Document PC Beep Hidden Register on Realtek ALC256
  ACK: pavel -- just a long documentation update
  - 44cc74947ce0 ALSA: pcm: oss: Fix regression by buffer overflow fix
  ACK: pavel
  - 8e68208928a5 ALSA: ice1724: Fix invalid access for enumerated ctl items
  ACK: pavel
  - b01126ec5309 ALSA: hda: Fix potential access overflow in beep helper
  ACK: pavel
  - d1fea655a639 ALSA: hda: Add driver blacklist
  ACK: pavel
  - 2754914b7ea0 ALSA: usb-audio: Add mixer workaround for TRX40 and co
  ACK: pavel
  - 6276915702fe usb: gadget: composite: Inform controller driver of self-powered
  ACK: pavel
  - 2ad5360690ab usb: gadget: f_fs: Fix use after free issue as part of queue failure
  ACK: pavel
  - 10848d3c8517 ASoC: topology: use name_prefix for new kcontrol
  ACK: pavel -- not sure if it fixes serious bug
  - e1093a170ffc ASoC: dpcm: allow start or stop during pause for backend
  ACK: pavel
  - 0185a432ad90 ASoC: dapm: connect virtual mux with default value
  ACK: pavel
  - 66f493d99a48 ASoC: fix regwmask
  ACK: pavel
  - 9ee0e501f804 slub: improve bit diffusion for freelist ptr obfuscation
  ACK: pavel
  - 9af535dc019e uapi: rename ext2_swab() to swab() and share globally in swab.h
  UR: pavel -- publish new function, not a bugfix; needed for next patch?
  - dce1622d5401 IB/mlx5: Replace tunnel mpls capability bits for tunnel_offloads
  ACK: pavel
  - 32fb859ec325 btrfs: track reloc roots based on their commit root bytenr
  ACK: pavel
  - 7d0ef6311f2d btrfs: remove a BUG_ON() from merge_reloc_roots()
  ACK: pavel
  - b8ab26fdcf7a btrfs: qgroup: ensure qgroup_rescan_running is only set when the worker is at least queued
  ACK: pavel
  - d999063be0cf block, bfq: fix use-after-free in bfq_idle_slice_timer_body
  ACK: pavel
  - c6090fe7883d locking/lockdep: Avoid recursion in lockdep_count_{for,back}ward_deps()
  ACK: pavel -- just a warning workaround
  - 1a5613b54b37 firmware: fix a double abort case with fw_load_sysfs_fallback
  ACK: pavel -- just a minor optimalization
  - 41778458dab6 md: check arrays is suspended in mddev_detach before call quiesce operations
  ACK: pavel
  - 702f64bc4e7e irqchip/gic-v4: Provide irq_retrigger to avoid circular locking dependency
  ACK: pavel
  - 831494cb2c1d usb: dwc3: core: add support for disabling SS instances in park mode
  UR: pavel -- check that the quirk is used?
  - 505e557cdb77 media: i2c: ov5695: Fix power on and off sequences
  ACK: pavel
  - cf535659b37d block: Fix use-after-free issue accessing struct io_cq
  ACK: pavel
  - 1b16ddb28b9a genirq/irqdomain: Check pointer in irq_domain_alloc_irqs_hierarchy()
  ACK: pavel -- robustness, not neccessarily a bugfix
  - f533f211babe efi/x86: Ignore the memory attributes table on i386
  ACK: pavel -- just a WARN() fix
  - 615d0014bff7 x86/boot: Use unsigned comparison for addresses
  ACK: pavel
  - 09f8ac747f36 gfs2: Don't demote a glock until its revokes are written
  ACK: pavel
  - 3e61c4fab13f pstore/platform: fix potential mem leak if pstore_init_fs failed
  ACK: pavel
  - 7371ef43c7e9 libata: Remove extra scsi_host_put() in ata_scsi_add_hosts()
  ACK: pavel
  - 6a63ea10f557 media: i2c: video-i2c: fix build errors due to 'imply hwmon'
  ACK: pavel
  - 12ce9fd7fc87 PCI/switchtec: Fix init_completion race condition with poll_wait()
  ACK: pavel
  - 5004f40bfbee selftests/x86/ptrace_syscall_32: Fix no-vDSO segfault
  ACK: pavel
  - 285162174712 sched: Avoid scale real weight down to zero
  ACK: pavel
  - a99fafc9aa25 irqchip/versatile-fpga: Handle chained IRQs properly
  ACK: pavel
  - fd397508347f block: keep bdi->io_pages in sync with max_sectors_kb for stacked devices
  ACK: pavel
  - 88d5a6fc582d x86: Don't let pgprot_modify() change the page encryption bit
  ACK: pavel
  - 2554ae0cc74e xhci: bail out early if driver can't accress host in resume
  ACK: pavel
  - fe5b2e54d6c6 null_blk: fix spurious IO errors after failed past-wp access
  ACK: pavel
  - 3e57e69bb3a6 null_blk: Handle null_add_dev() failures properly
  ACK: pavel
  - d1964461cf48 null_blk: Fix the null_add_dev() error path
  ACK: pavel
  - 0dbab95acc8c firmware: arm_sdei: fix double-lock on hibernate with shared events
  ACK: pavel
  - 7f669684465d media: venus: hfi_parser: Ignore HEVC encoding for V1
  ACK: pavel
  - 188d564270dc cpufreq: imx6q: Fixes unwanted cpu overclocking on i.MX6ULL
  ACK: pavel
  - 9f18f4a61bcd i2c: st: fix missing struct parameter description
  ACK: pavel -- just a documentation fix
  - 8c80608a4eef qlcnic: Fix bad kzalloc null test
  ACK: pavel
  - 0c36582cf87c cxgb4/ptp: pass the sign of offset delta in FW CMD
  ACK: pavel
  - 5b274de8f45f hinic: fix wrong para of wait_for_completion_timeout
  ACK: pavel
  - a856a90ad252 hinic: fix a bug of waitting for IO stopped
  ACK: pavel
  - 565fcc44698e net: vxge: fix wrong __VA_ARGS__ usage
  ACK: pavel -- theoretical error, probably does not happen in current stable
  - f615ab435e2d bus: sunxi-rsb: Return correct data when mixing 16-bit and 8-bit reads
  UR: pavel -- explicitely says not for stable
  - 09f8a617bea8 ARM: dts: sun8i-a83t-tbs-a711: HM5065 doesn't like such a high voltage
  ACK: pavel
