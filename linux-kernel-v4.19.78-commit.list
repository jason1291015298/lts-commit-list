# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.78
  - 58fce2064530 Linux 4.19.78
  - 5b0446c8e0a8 9p/cache.c: Fix memory leak in v9fs_cache_session_get_cookie
  ACK: pavel
  - d85bc11a6894 kexec: bail out upon SIGKILL when allocating memory.
  ACK: pavel
  - c8a65ec0602a NFC: fix attrs checks in netlink interface
  ACK: pavel
  - 1b42503211ca smack: use GFP_NOFS while holding inode_smack::smk_lock
  ACK: pavel
  - ef9744a0218f Smack: Don't ignore other bprm->unsafe flags if LSM_UNSAFE_PTRACE is set
  ACK: pavel
  - 4703593498d3 soundwire: fix regmap dependencies and align with other serial links
  ACK: pavel -- user visible kconfig change, not a bugfix
  - 322753c78d1d soundwire: Kconfig: fix help format
  ACK: pavel -- cleanup, not a bugfix
  - 74e2a311a226 sch_cbq: validate TCA_CBQ_WRROPT to avoid crash
  ACK: pavel
  - ed9420ddce87 tipc: fix unlimited bundling of small messages
  ACK: pavel
  - a1afd826e549 xen-netfront: do not use ~0U as error return value for xennet_fill_frags()
  ACK: pavel
  - 36a4043c4bb8 net/rds: Fix error handling in rds_ib_add_one()
  ACK: pavel
  - 012363f5ded0 udp: only do GSO if # of segs > 1
  ACK: pavel -- makes behaviour consistent, not serious bug
  - 5c08d7e4f1ea net: dsa: rtl8366: Check VLAN ID and not ports
  ACK: pavel -- could use helper function, vlan_add should probably return an error
  - 3c1f0704401c vsock: Fix a lockdep warning in __vsock_release()
  ACK: pavel
  - 544aee546174 udp: fix gso_segs calculations
  ACK: pavel
  - 79fd59ae7c2f sch_dsmark: fix potential NULL deref in dsmark_init()
  ACK: pavel
  - 76b552775d60 rxrpc: Fix rxrpc_recvmsg tracepoint
  ACK: pavel
  - 7047aae65456 qmi_wwan: add support for Cinterion CLS8 devices
  ACK: pavel
  - dd9c580a6d9b nfc: fix memory leak in llcp_sock_bind()
  ACK: pavel
  - d5b1db1c7ce4 net: Unpublish sk from sk_reuseport_cb before call_rcu
  ACK: pavel
  - 9d0995cc268b net: qlogic: Fix memory leak in ql_alloc_large_buffers
  ACK: pavel
  - 124b64feafa9 net: ipv4: avoid mixed n_redirects and rate_tokens usage
  ACK: pavel
  - 6f8564edf8c1 ipv6: Handle missing host route in __ipv6_ifa_notify
  ACK: pavel
  - 658d7ee4da50 ipv6: drop incoming packets having a v4mapped source address
  ACK: pavel
  - a495fd19cce2 hso: fix NULL-deref on tty open
  ACK: pavel
  - 7f30c44b7ca4 erspan: remove the incorrect mtu limit for erspan
  ACK: pavel
  - 2b8389112292 cxgb4:Fix out-of-bounds MSI-X info array access
  ACK: pavel
  - ed568ca73601 bpf: fix use after free in prog symbol exposure
  ACK: pavel
  - dbb7339cfddf block: mq-deadline: Fix queue restart handling
  ACK: pavel
  - af10ffa69b06 arm: use STACK_TOP when computing mmap base address
  ACK: pavel
  - f91a9c6591c0 arm: properly account for stack randomization and stack guard gap
  ACK: pavel -- known bad, fixed by next patch
  - 53ba8d4358eb mips: properly account for stack randomization and stack guard gap
  ACK: pavel -- whitespace change as a bonus
  - e1b391abbfce arm64: consider stack randomization for mmap base only when necessary
  ACK: pavel
  - 30ab799e758e kmemleak: increase DEBUG_KMEMLEAK_EARLY_LOG_SIZE default to 16K
  ACK: pavel -- just a change of Kconfig default, will not affect existing users
  - 52132ff52cad ocfs2: wait for recovering done after direct unlock request
  ACK: pavel
  - d4a546452dcc kbuild: clean compressed initramfs image
  ACK: pavel
  - d983182d4002 crypto: hisilicon - Fix double free in sec_free_hw_sgl()
  ACK: pavel
  - 22c788ba7a52 hypfs: Fix error number left in struct pointer member
  IGN: pavel
  - bbd76d9514c7 pktcdvd: remove warning on attempting to register non-passthrough dev
  ACK: pavel -- just a warning removal
  - 0840daeea6cb fat: work around race with userspace's read via blockdev while mounting
  ACK: pavel
  - 297904ea60a3 ARM: 8903/1: ensure that usable memory in bank 0 starts from a PMD-aligned address
  ACK: pavel
  - 9a87ab2b4d60 security: smack: Fix possible null-pointer dereferences in smack_socket_sock_rcv_skb()
  ACK: pavel -- theoretical bug
  - 69a32a7306dd PCI: exynos: Propagate errors for optional PHYs
  NAK: pavel -- does not fix a bug, has potential for regressions
  - 1264d2e7b75b PCI: imx6: Propagate errors for optional regulators
  NAK: pavel -- does not fix a bug, has potential for regressions
  - 403d6c9284f1 PCI: histb: Propagate errors for optional regulators
  NAK: pavel -- does not fix a bug, has potential for regressions
  - ac9c0e2ecc49 PCI: rockchip: Propagate errors for optional regulators
  NAK: pavel -- does not fix a bug, has potential for regressions
  - 709c4841e51f HID: apple: Fix stuck function keys when using FN
  ACK: pavel
  - 31e98cba55ed rtc: pcf85363/pcf85263: fix regmap error in set_time
  ACK: pavel
  - 607f95788fa9 rtc: snvs: fix possible race condition
  ACK: pavel
  - 078937549f47 ARM: 8875/1: Kconfig: default to AEABI w/ Clang
  ACK: pavel -- kconfig default tweak, not a bugfix
  - 3039c788d648 soundwire: intel: fix channel number reported by hardware
  ACK: pavel
  - 6a684e002561 ARM: 8898/1: mm: Don't treat faults reported from cache maintenance as writes
  ACK: pavel
  - 0f0ced702d53 livepatch: Nullify obj->mod in klp_module_coming()'s error path
  ACK: pavel
  - 78a1138672af HID: wacom: Fix several minor compiler warnings
  ACK: pavel -- supposed to be just a warning fix, but they modify buffer sizes, too
  - 3b7fbbddeb7a PCI: tegra: Fix OF node reference leak
  ACK: pavel -- just a tiny memory leak
  - c74a801ded7d mfd: intel-lpss: Remove D3cold delay
  ACK: pavel
  - 70bb4bf9fdfb i2c-cht-wc: Fix lockdep warning
  ACK: pavel
  - 371077ea2e75 MIPS: tlbex: Explicitly cast _PAGE_NO_EXEC to a boolean
  ACK: pavel
  - 3ed14a8d2fe7 MIPS: Ingenic: Disable broken BTB lookup optimization.
  UR: pavel -- sounds like a big hammer to "fix" bogomips... 
  - 5b400fed74df ext4: fix potential use after free after remounting with noblock_validity
  ACK: pavel
  - 81fbd2327c32 dma-buf/sw_sync: Synchronize signal vs syncpt free
  ACK: pavel
  - c76e18970d93 scsi: core: Reduce memory required for SCSI logging
  ACK: pavel -- simplifies code a lot but... why was it written like this in a first place?
  - c6304d4d7458 clk: sprd: add missing kfree
  ACK: pavel
  - 7cd89b8db60c mbox: qcom: add APCS child device for QCS404
  ACK: pavel
  - 324b0c9efc77 powerpc: dump kernel log before carrying out fadump or kdump
  IGN: pavel
  - 72884423e77f clk: at91: select parent if main oscillator or bypass is enabled
  ACK: pavel -- inline function might be better
  - 952d1c6d7cf0 arm64: fix unreachable code issue with cmpxchg
  ACK: pavel
  - b25bd837a637 pinctrl: meson-gxbb: Fix wrong pinning definition for uart_c
  ACK: pavel
  - b717a47dd913 powerpc/pseries: correctly track irq state in default idle
  IGN: pavel
  - ae089bf87c57 clk: qcom: gcc-sdm845: Use floor ops for sdcc clks
  ACK: pavel
  - 2c16f72135fb pstore: fs superblock limits
  ACK: pavel
  - 0c09b02842b8 powerpc/64s/exception: machine check use correct cfar for late handler
  IGN: pavel
  - 39b6d05169b2 drm/amdgpu/si: fix ASIC tests
  ACK: pavel -- not sure if it was tested and fixes a real bug
  - 4dcbca872a84 drm/amd/display: support spdif
  NAK: pavel -- changelog does not make sense, code does not make sense either
  - 38dfc974f322 clk: renesas: cpg-mssr: Set GENPD_FLAG_ALWAYS_ON for clock domain
  ACK: pavel
  - 0b5ac607db4b clk: renesas: mstp: Set GENPD_FLAG_ALWAYS_ON for clock domain
  ACK: pavel -- just fixes a warning during bootup
  - 2cfb89832179 pinctrl: amd: disable spurious-firing GPIO IRQs
  ACK: pavel
  - 274d7acb0b81 drm/nouveau/volt: Fix for some cards having 0 maximum voltage
  ACK: pavel
  - 9b2d2f2ad003 vfio_pci: Restore original state on release
  ACK: pavel
  - c1f7b3fb87cf powerpc/eeh: Clear stale EEH_DEV_NO_HANDLER flag
  IGN: pavel
  - b2df456c83e3 pinctrl: tegra: Fix write barrier placement in pmx_writel
  ACK: pavel
  - 4c91e678d27c powerpc/pseries/mobility: use cond_resched when updating device tree
  IGN: pavel
  - 6d728a172732 powerpc/futex: Fix warning: 'oldval' may be used uninitialized in this function
  IGN: pavel
  - 6aa455b0d0a0 powerpc/rtas: use device model APIs and serialization during LPM
  IGN: pavel
  - 25c501f0f9d9 powerpc/xmon: Check for HV mode when dumping XIVE info from OPAL
  IGN: pavel
  - 2cca24b2cb9a clk: zx296718: Don't reference clk_init_data after registration
  NAK: pavel -- preparation for future change, not a bugfix
  - efa0fe4cde05 clk: sprd: Don't reference clk_init_data after registration
  NAK: pavel -- preparation for future change, not a bugfix
  - 89dc59fb267d clk: sirf: Don't reference clk_init_data after registration
  NAK: pavel -- preparation for future change, not a bugfix
  - bd3a445c2717 clk: actions: Don't reference clk_init_data after registration
  NAK: pavel -- preparation for future change, not a bugfix
  - 437399ed906a powerpc/powernv/ioda2: Allocate TCE table levels on demand for default DMA window
  IGN: pavel
  - 782a77f2eb39 drm/amd/display: reprogram VM config when system resume
  ACK: pavel
  - 24ba84ec0016 drm/amd/display: fix issue where 252-255 values are clipped
  ACK: pavel
  - efb0e1e3d0e1 clk: sunxi-ng: v3s: add missing clock slices for MMC2 module clocks
  ACK: pavel
  - a2279550f7be clk: qoriq: Fix -Wunused-const-variable
  ACK: pavel
  - 84038a98b904 ipmi_si: Only schedule continuously in the thread in maintenance mode
  NAK: pavel -- performance tweak, not a bugfix; probably should use cond_resched, certainly should not use usleep_range()
  - b351726bb5be PCI: rpaphp: Avoid a sometimes-uninitialized warning
  - 0936c46139cb gpu: drm: radeon: Fix a possible null-pointer dereference in radeon_connector_set_property()
  ACK: pavel -- theoretical bug
  - 6e03bca91f8e drm/radeon: Fix EEH during kexec
  ACK: pavel -- misformated comments, whitespace damage
  - 441c15582338 drm/rockchip: Check for fast link training before enabling psr
  ACK: pavel
  - f3d62177dee3 drm/panel: check failure cases in the probe func
  ACK: pavel -- just a tiny memory leak
  - 9cb3698dcc86 drm/stm: attach gem fence to atomic state
  ACK: pavel
  - 043f0229f442 video: ssd1307fb: Start page range at page_offset
  ACK: pavel
  - bd5b6a7c89ef drm/panel: simple: fix AUO g185han01 horizontal blanking
  ACK: pavel
  - db472be8b340 drm/bridge: tc358767: Increase AUX transfer length limit
  ACK: pavel -- speedup, not a bugfix
  - 053d0ec61e33 tpm: Fix TPM 1.2 Shutdown sequence to prevent future TPM operations
  ACK: pavel -- still looks mismerged, contains statement with no effect
  - d598712712e4 tpm: use tpm_try_get_ops() in tpm-sysfs.c.
  NAK: pavel -- not a bugfix, redoes locking completely..
