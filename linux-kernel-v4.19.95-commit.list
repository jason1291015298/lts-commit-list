# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.95
  - dcd888983542 Linux 4.19.95
  - aad8003ad201 usb: missing parentheses in USE_NEW_SCHEME
  UR: pavel -- wtf? Code is very confusing
  - a16ae61375c0 USB: serial: option: add Telit ME910G1 0x110a composition
  ACK: pavel
  - f85541a5d8d5 USB: core: fix check for duplicate endpoints
  UR: pavel
  - ceaeb21b4bda usb: dwc3: gadget: Fix request complete check
  ACK: pavel
  - 672d3ca2f395 net: sch_prio: When ungrafting, replace with FIFO
  ACK: pavel
  - 741cf884069e mlxsw: spectrum_qdisc: Ignore grafting of invisible FIFO
  ACK: pavel -- just an error handling improvement
  - a98672dd8f79 vlan: vlan_changelink() should propagate errors
  UR: pavel -- not sure what user visible bug it fixes; old code set egress priority even in case of errors
  - 4e71a156b40d vlan: fix memory leak in vlan_dev_set_egress_priority
  ACK: pavel
  - b5fdf59c2019 vxlan: fix tos value before xmit
  ACK: pavel
  - 5ea5b6e3c6e1 tcp: fix "old stuff" D-SACK causing SACK to be treated as D-SACK
  ACK: pavel
  - 5f52b9ebd47c sctp: free cmd->obj.chunk for the unprocessed SCTP_CMD_REPLY
  ACK: pavel
  - f5c8c211d811 sch_cake: avoid possible divide by zero in cake_enqueue()
  ACK: pavel
  - 94ac4a4d938f pkt_sched: fq: do not accept silly TCA_FQ_QUANTUM
  ACK: pavel -- "interesting" fix
  - 77b07bbf47e4 net: usb: lan78xx: fix possible skb leak
  ACK: pavel
  - 5994f91dcdc4 net: stmmac: dwmac-sunxi: Allow all RGMII modes
  ACK: pavel
  - 527d74630691 net: stmmac: dwmac-sun8i: Allow all RGMII modes
  ACK: pavel
  - d36857e02bf8 net: dsa: mv88e6xxx: Preserve priority when setting CPU port.
  ACK: pavel
  - 5f3274c53ae7 macvlan: do not assume mac_header is set in macvlan_broadcast()
  ACK: pavel
  - 776a81a024e7 gtp: fix bad unlock balance in gtp_encap_enable_socket
  ACK: pavel
  - 13d9f98ef4c1 PCI/switchtec: Read all 64 bits of part_event_bitmap
  ACK: pavel
  - 40b049d085f3 ARM: dts: imx6ul: use nvmem-cells for cpu speed grading
  ACK: pavel -- depends on previous fix, may prevent new dtb from working with old kernel
  - 4ef576e99d29 cpufreq: imx6q: read OCOTP through nvmem for imx6ul/imx6ull
  UR: pavel
  - 0b9700de712a powerpc/spinlocks: Include correct header for static key
  ACK: pavel
  - 04a2096c4bf0 powerpc/vcpu: Assume dedicated processors as non-preempt
  IGN: pavel -- just a performance fix
  - 24fc85b5f5b6 hv_netvsc: Fix unwanted rx_table reset
  ACK: pavel
  - fc95d2b0d4ee llc2: Fix return statement of llc_stat_ev_rx_null_dsap_xid_c (and _test_c)
  ACK: pavel
  - 4b9f0187aa07 parisc: Fix compiler warnings in debug_core.c
  ACK: pavel -- just a warning fix
  - e80e36de03f3 block: fix memleak when __blk_rq_map_user_iov() is failed
  ACK: pavel
  - 729a7f5e41f3 s390/dasd: fix memleak in path handling error case
  IGN: pavel
  - 000d4f2fcf2c s390/dasd/cio: Interpret ccw_device_get_mdc return value correctly
  IGN: pavel
  - f2bcbc595849 drm/exynos: gsc: add missed component_del
  ACK: pavel
  - 152eaa8626cc s390/purgatory: do not build purgatory with kcov, kasan and friends
  IGN: pavel
  - 5ee6f0da6bbd net: stmmac: Always arm TX Timer at end of transmission start
  ACK: pavel
  - c8fa1cb10330 net: stmmac: RX buffer size must be 16 byte aligned
  ACK: pavel -- theoretical problem, as cache sizes are > 16 bytes on most hw
  - 864a8ff571d2 net: stmmac: xgmac: Clear previous RX buffer size
  ACK: pavel
  - 57d547a41ecf net: stmmac: Do not accept invalid MTU values
  ACK: pavel -- just a better error checking
  - 77de8ee6b09f fs: avoid softlockups in s_inodes iterators
  ACK: pavel -- just a softlockup warning fix, not a minmal one
  - 50360686bf73 perf/x86/intel: Fix PT PMI handling
  ACK: pavel
  - a64d7894b461 kconfig: don't crash on NULL expressions in expr_eq()
  ACK: pavel -- fixes kconfig failure, not a serious bug
  - 17a6e26e5474 iommu/iova: Init the struct iova to fix the possible memleak
  ACK: pavel
  - b82c08e490c0 regulator: rn5t618: fix module aliases
  ACK: pavel -- just fixes autoloading
  - 18932974d67b ASoC: wm8962: fix lambda value
  ACK: pavel -- not sure if it fixes concrete bug
  - d8ec4cf103d6 rfkill: Fix incorrect check to avoid NULL pointer dereference
  NAK: pavel -- rationale is incorrect, not a minimum fix, no reason this would happen in practice
  - 4afc044f2010 parisc: add missing __init annotation
  ACK: pavel
  - 588b700ce88b net: usb: lan78xx: Fix error message format specifier
  ACK: pavel -- just a printk improvement
  - 93aaacd3da74 cxgb4: Fix kernel panic while accessing sge_info
  ACK: pavel
  - 133c0bab9437 bnx2x: Fix logic to get total no. of PFs per engine
  ACK: pavel
  - 1c96c9f1470b bnx2x: Do not handle requests from VFs after parity
  ACK: pavel
  - e3d5f12b312a bpf: Clear skb->tstamp in bpf_redirect when necessary
  ACK: pavel
  - 12ac1eb6ee76 btrfs: Fix error messages in qgroup_rescan_init
  ACK: pavel -- just a printk fix
  - 055b4a62f115 powerpc: Ensure that swiotlb buffer is allocated from low memory
  ACK: pavel
  - 7d5ad9fe2c9b samples: bpf: fix syscall_tp due to unused syscall
  ACK: pavel -- just example code
  - 5663eb75ee9a samples: bpf: Replace symbol compare of trace_event
  ACK: pavel -- just example code
  - 712cd9d02b84 ARM: dts: am437x-gp/epos-evm: fix panel compatible
  UR: pavel
  - 5821a1e91e0f spi: spi-ti-qspi: Fix a bug when accessing non default CS
  ACK: pavel
  - e240f26a7f1d bpf, mips: Limit to 33 tail calls
  IGN: pavel
  - 4bf973a1f84a bnxt_en: Return error if FW returns more data than dump length
  UR: pavel -- check error exits
  - d1611ab68772 ARM: dts: bcm283x: Fix critical trip point
  ACK: pavel
  - 459e1fba92b6 ASoC: topology: Check return value for soc_tplg_pcm_create()
  ACK: pavel -- just error checking improvement, not sure it fixes anything
  - f8832728bf33 spi: spi-cavium-thunderx: Add missing pci_release_regions()
  ACK: pavel
  - 59b1cbee0885 ARM: dts: Cygnus: Fix MDIO node address/size cells
  ACK: pavel -- just a warning fix
  - db3cb5c11ae7 selftests/ftrace: Fix multiple kprobe testcase
  ACK: pavel
  - 6e168ef21fa3 ARM: dts: BCM5301X: Fix MDIO node address/size cells
  ACK: pavel -- just a warning fix
  - 84133b6011ee netfilter: nf_tables: validate NFT_DATA_VALUE after nft_data_init()
  ACK: pavel
  - dc330d942c9f netfilter: nf_tables: validate NFT_SET_ELEM_INTERVAL_END
  ACK: pavel
  - 80a035ebefa1 netfilter: nft_set_rbtree: bogus lookup/get on consecutive elements in named sets
  ACK: pavel
  - 962debec8313 netfilter: uapi: Avoid undefined left-shift in xt_sctp.h
  ACK: pavel -- not sure it causes real bug
  - 829fde079bd9 ARM: vexpress: Set-up shared OPP table instead of individual for each CPU
  pavel -- does it fix a bug?
  - 05f3fbb05380 ARM: dts: imx6ul: imx6ul-14x14-evk.dtsi: Fix SPI NOR probing
  ACK: pavel
  - 0010192db178 efi/gop: Fix memory leak in __gop_query32/64()
  UR: pavel
  - 737bc8f67879 efi/gop: Return EFI_SUCCESS if a usable GOP was found
  ACK: pavel
  - 765a2970e66f efi/gop: Return EFI_NOT_FOUND if there are no usable GOPs
  ACK: pavel
  - 0320cc64e325 ASoC: Intel: bytcr_rt5640: Update quirk for Teclast X89
  ACK: pavel
  - fa03b9cab91c x86/efi: Update e820 with reserved EFI boot services data to fix kexec breakage
  ACK: pavel
  - cb3b83a1151b libtraceevent: Fix lib installation with O=
  ACK: pavel -- just a build problem in non-standard situation
  - 21f08020dd85 mwifiex: Fix heap overflow in mmwifiex_process_tdls_action_frame()
  ACK: pavel
  - 9e0f4d24733e netfilter: ctnetlink: netns exit must wait for callbacks
  ACK: pavel
  - c7673f01604f locking/spinlock/debug: Fix various data races
  ACK: pavel
  - 5a8c4f96a5f0 ASoC: max98090: fix possible race conditions
  ACK: pavel
  - 551d7ff754f5 regulator: fix use after free issue
  ACK: pavel
  - f70280ee8937 bpf: Fix passing modified ctx to ld/abs/ind instruction
  ACK: pavel
  - 11c6893b1b23 USB: dummy-hcd: increase max number of devices to 32
  ACK: pavel -- configuration tweak, not a bugfix
  - 41f78c38ed9c USB: dummy-hcd: use usb_urb_dir_in instead of usb_pipein
  ACK: pavel
