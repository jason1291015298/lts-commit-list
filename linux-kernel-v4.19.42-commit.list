# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.42
  - 9c2556f428cf Linux 4.19.42
  - 9ccdbde1850c arm64: futex: Bound number of LDXR/STXR loops in FUTEX_WAKE_OP
  ACK: pavel
  - 0f4ef8fb1dae locking/futex: Allow low-level atomic operations to return -EAGAIN
  pavel -- too complex -- does not match stable rules
  - 6fee39874d1f ASoC: Intel: avoid Oops if DMA setup fails
  ACK: pavel
  - c3b3955f0250 UAS: fix alignment of scatter/gather segments
  ACK: pavel -- diff has whitespace problems
  - 38f092c41ceb Bluetooth: Align minimum encryption key size for LE and BR/EDR connections
  pavel -- not sure what bug it fixes
  - c6d1f9b4b2cb Bluetooth: hidp: fix buffer overflow
  ACK: pavel
  - de7fe08b92dc scsi: qla2xxx: Fix device staying in blocked state
  ACK: pavel
  - ef7014d76361 scsi: qla2xxx: Fix incorrect region-size setting in optrom SYSFS routines
  ACK: pavel
  - 7a793ca17357 scsi: lpfc: change snprintf to scnprintf for possible overflow
  pavel -- much too big, fixes cases that can not happen
  - 8b330b3efa91 soc: sunxi: Fix missing dependency on REGMAP_MMIO
  ACK: pavel
  - 557be5771563 cpufreq: armada-37xx: fix frequency calculation for opp
  ACK: pavel
  - 6b7daf1ff8bf intel_th: pci: Add Comet Lake support
  ACK: pavel
  - 5b2ba94386eb usb-storage: Set virt_boundary_mask to avoid SG overflows
  ACK: pavel
  - 18e6f3027642 USB: cdc-acm: fix unthrottle races
  ACK: pavel
  - bce2b9d63786 USB: serial: f81232: fix interrupt worker not stop
  pavel -- two fixes in one
  - caa5680dc05a usb: dwc3: Fix default lpm_nyet_threshold value
  ACK: pavel
  - 33f2aa87c294 genirq: Prevent use-after-free and work list corruption
  ACK: pavel
  - 29184cbaaec0 iommu/amd: Set exclusion range correctly
  - 42638d6aae06 perf/core: Fix perf_event_disable_inatomic() race
  - c1189d68be7d platform/x86: pmc_atom: Drop __initconst on dmi table
  ACK: pavel
  - 777943cd6c5f nvme-fc: correct csn initialization and increments on error
  - 0e8e67b8147f virtio-blk: limit number of hw queues by nr_cpu_ids
  - d955bb0b3189 ASoC: Intel: kbl: fix wrong number of channels
  ACK: pavel
  - e5c749ad6d7f drm/mediatek: fix possible object reference leak
  pavel -- insignificant memory leak
  - 8f4dbd17777f scsi: csiostor: fix missing data copy in csio_scsi_err_handler()
  - fb357b9eb47d RDMA/hns: Fix bug that caused srq creation to fail
  ACK: pavel
  - 8dfb2896d8c7 RDMA/vmw_pvrdma: Fix memory leak on pvrdma_pci_remove
  pavel -- memory leak in remove routine. Serious enough?
  - 5984fd687600 virtio_pci: fix a NULL pointer reference in vp_del_vqs
  ACK: pavel
  - a8f5c1bceb25 drm/sun4i: tcon top: Fix NULL/invalid pointer dereference in sun8i_tcon_top_un/bind
  ACK: pavel
  - 78bc98235e84 slab: fix a crash by reading /proc/slab_allocators
  ACK: pavel
  - cf6cb79d57b0 objtool: Add rewind_stack_do_exit() to the noreturn list
  pavel -- just a warning
  - e66e72710962 ASoC: cs35l35: Disable regulators on driver removal
  pavel -- just a warning
  - dd015a3b072a drm/amd/display: fix cursor black issue
  pavel
  - 88294658ddbe ASoC: rockchip: pdm: fix regmap_ops hang issue
  ACK: pavel
  - 95587274e9d5 linux/kernel.h: Use parentheses around argument in u64_to_user_ptr()
  pavel -- as buggy user of the macro is not in tree, it is theoretical bug
  - 7d10436ca569 perf/x86/intel: Initialize TFA MSR
  ACK: pavel
  - 2b791e8ee7b2 perf/x86/intel: Fix handling of wakeup_events for multi-entry PEBS
  ACK: pavel
  - 929d019d6d44 drm/mediatek: Fix an error code in mtk_hdmi_dt_parse_pdata()
  ACK: pavel
  - 0fb785e28833 ASoC: tlv320aic32x4: Fix Common Pins
  ACK: pavel
  - e6efcbf3cbce MIPS: KGDB: fix kgdb support for SMP platforms.
  ACK: pavel
  - 09c6954e83e8 IB/hfi1: Fix the allocation of RSM table
  pavel -- variable rename makes it hard to understand
  - 3abd4aef917f IB/hfi1: Eliminate opcode tests on mr deref
  ACK: pavel
  - 1ed91af83a45 drm/omap: hdmi4_cec: Fix CEC clock handling for PM
  pavel -- power-management optimalization
  - d356db088333 ASoC: dapm: Fix NULL pointer dereference in snd_soc_dapm_free_kcontrol
  - 6f69661f6ebe ASoC: cs4270: Set auto-increment bit for register writes
  - 8f5077ceee5f ASoC: stm32: dfsdm: fix debugfs warnings on entry creation
  - 33ffe0807d52 ASoC: stm32: dfsdm: manage multiple prepare
  - 74f5898f660d clk: meson-gxbb: round the vdec dividers to closest
  pavel -- concrete bug is not described
  - 8aa62dc731e5 ASoC: wm_adsp: Add locking to wm_adsp2_bus_error
  - 2ece73fe079b ASoC: rt5682: recording has no sound after booting
  - afcbb3c755c9 ASoC: samsung: odroid: Fix clock configuration for 44100 sample rate
  - 7525d6104ca4 ASoC: nau8810: fix the issue of widget with prefixed name
  - c2119de4ed41 ASoC: nau8824: fix the issue of the widget with prefix name
  - 29f1b9761520 ASoC:intel:skl:fix a simultaneous playback & capture issue on hda platform
  ACK: pavel -- typo: inaudile
  - 581a8bd9fa8b ASoC:soc-pcm:fix a codec fixup issue in TDM case
  ACK: pavel
  - c37f7344075a ASoC: stm32: sai: fix exposed capabilities in spdif mode
  pavel -- adds support to S/PDIF
  - 2aeceaaff09b ASoC: stm32: sai: fix iec958 controls indexation
  pavel -- not a bugfix.
  - 6544b4966193 ASoC: hdmi-codec: fix S/PDIF DAI
  pavel -- rewrite, not a bugfix. Preparation for something?
  - 98a80393b82a ASoC: tlv320aic3x: fix reset gpio reference counting
  ACK: pavel -- wrong reference counting, not terribly serious; noone unloads these anyway
  - 34ae4c6a3609 staging: most: cdev: fix chrdev_region leak in mod_exit
  - 2197e11bb624 staging: greybus: power_supply: fix prop-descriptor request size
  ACK: pavel -- "))" in changelog
  - 35d2c86db2d4 ubsan: Fix nasty -Wbuiltin-declaration-mismatch GCC-9 warnings
  pavel
  - eb7b8d1afb92 Drivers: hv: vmbus: Remove the undesired put_cpu_ptr() in hv_synic_cleanup()
  ACK: pavel
  - 0f18e433b97b scsi: libsas: fix a race condition when smp task timeout
  ACK: pavel
  - e629cabe34ed net: stmmac: Use bfsize1 in ndesc_init_rx_desc
  ACK: pavel
